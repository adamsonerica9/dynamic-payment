<?php
	session_start();

	$page = "aboutUs";

	include "phpclass/util.php";
	include "lang.php";
	
	// Read templates
	$templateIndex = $util->readTemplate("lang/template/about_us.php");
	
	// Read language json
	$langIndex = $util->readLang("lang/about_us.json");
	
	/** 
	 * 	Page content
	*/
	$templateIndex = $util->langTemplate($templateIndex, $langIndex, $lang);

	$display = $util->template($templateIndex, $templateData);
	
	echo $display;

?>