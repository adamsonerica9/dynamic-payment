<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">Dynamic Payment Prepaid Card</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
    <div class="right_content page_t">
      <h3>Prepaid Card Flow Overview</h3>

      <div class="row">
                
        <div class="col-md-6" style="text-align:center;">
          <img src="images/workflow1.png" alt="Card Management" width="110%">
          <img src="images/workflow1a.png" alt="Test Automation" width="110%">
          <img src="images/cardholder_vertification.png" alt="Cardholder Vertification" width="110%">
          <img src="images/workflowarrow1a.png" alt="Arrow Down" width="110%">
          <img src="images/workflow1b.png" alt="DP UnionPay Card" width="110%">
          <img src="images/dp_wallet_app_link_up_with_dp_prepaid_card.png" alt="DP Wallet APP link up with DP Prepaid Card" width="110%">
          <img src="images/workflowarrow1b.png" alt="Arrow Down" width="110%">
          <img src="images/workflow1c.png" alt="Scan Card" width="110%">
          <img src="images/top_up.png" alt="Top Up" width="110%">
        </div>

        <div class="col-md-6" style="text-align:center;">
          <img src="images/workflow2.png" alt="Card Payment" width="110%">
          <img src="images/workflow2a.png" alt="Scan QR Code" width="110%">
          <img src="images/dp_wallet_app_scan_QR_code_or_NFC.png" alt="DP Wallet APP Scan QR code/NFC (Android phone)" width="110%">
          <img src="images/workflowarrow2a.png" alt="Arrow Down" width="110%">
          <img src="images/workflow2b.png" alt="Card Management" width="110%">
          <img src="images/card_top_insert_or_swipe.png" alt="Card Top. Insert or Swipe" width="110%">
          <img src="images/workflowarrow2b.png" alt="Arrow Down" width="110%">
          <img src="images/workflow2c.png" alt="Handle Parcel" width="110%">      
          <img src="images/transaction_completed.png" alt="Transaction Completed" width="110%">       
        </div>

      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="divider"></div>
  </div>
</section>
<?php
  include("footer.php");
?>

<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>
</body>
</html>
