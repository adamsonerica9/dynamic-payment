<div id="fh5co-footer" role="contentinfo">
  <div class="container ff-width">
    <div class="row">
      <div class="col-md-12">
        <div class="gtco-widget">
          <h3>Follow Us</h3>
          <div class="social-box"> <a href="#"><img src="../../../images/wechat.svg" alt="wechat icon" /></a> <a href="https://linkedin.com/company/dynamic-payment" target="_blank"><img src="../../../images/linkedin.svg" alt="Linkedin icon" /></a> </div>
        </div>
        <img class="foot_logo" src="../../../images/logo_bw.svg" alt="background Logo" />
        <p>© 2023 Dynamic Payment Inc. All rights reserved.</p>
      </div>
    </div>
  </div>
</div>