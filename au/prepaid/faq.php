<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">FAQ & HELP</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
    <div class="right_content page_t">
      <h3>FAQ & HELP</h3>
      <p><b>Q: Where can buy the physical card and virtual card?</b><br>
      A: Physical Cards are sold at our authorized stores and virtual Cards can be purchased online at here.</p>
	  <br>
      <p><b>Q: How to activate the card?</b><br>
      A: You need to activate the Card you purchased before it can be used. You will need to go online at <a href="https://scis.unionpayintl.com/h5scis/42870036/#/">https://scis.unionpayintl.com/h5scis/42870036/#/</a> and follow the instruction to activate the Card.</p>
	  <br>
      <p><b>Q: How to download the DP Wallet?</b><br>
      A: Search "DP Wallet Global" in AppStore or Google Play on your mobile devices and press download button to get the DP Wallet</p>
	  <br>
      <p><b>Q: Where can I use the prepaid card?</b><br>
      A: The Card can be used for purchases of goods and services, where UnionPay prepaid cards are accepted, including at online merchants who accept UnionPay Online Payment (UPOP). However, some merchants may impose their own conditions on the use of the Card, such as minimum payment amounts.</p>
	  <br>
      <p><b>Q: How to do the payment with DP Wallet?</b><br>
      A: You can use your virtual Card for payment of online transactions and for payments through DP Wallet by linking the virtual Card to your DP Wallet. When you pay via DP Wallet, present the QR code or tap your phone with the merchants terminal.</p>
	  <br>
      <p><b>Q: What is the service hotline?</b><br>
      A: Please call +612 9261 0108</p>
    </div>
    <div class="clearfix"></div>
  </div>
</section>
<?php
  include("footer.php");
?>

<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>
</body>
</html>
