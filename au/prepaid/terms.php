<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">Terms & Conditions</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
    <div class="right_content page_t">

      <h3>UnionPay Gift Card Terms and Conditions</h3>
	  
      <p>The UnionPay Gift Card ('Card') is issued by Dynamic Payment Pty Ltd ("DP") pursuant to license by UnionPay International. By purchasing or using the Card you agree to the terms and conditions stated herein ("Terms"). In these Terms, 'you' means the Card purchaser or user. </p>
      <ul class="f_lis">
		<li>
            <b>Stored value:</b>
            The Card is a prepaid card loaded with specific amount of funds when you purchased it. It cannot be reloaded with additional value. Physical Cards are sold at our authorized stores and virtual Cards can be purchased online at: <a target='_blank' href="https://www.dynamic-payment.com/au/prepaid/orderEnquiry.html">https://www.dynamic-payment.com/au/prepaid/orderEnquiry.html</a>
		</li>
        
        <li>
            <b>Activation:</b>
            You need to activate the Card you purchased before it can be used. You will need to go online at: <a target='_blank' href="https://www.dynamic-payment.com/au/prepaid/activate.html">https://www.dynamic-payment.com/au/prepaid/activate.html</a> and follow the instruction to activate the Card. 
		</li>
		
        <li>
            <b>PIN:</b>
            When you activate your Card online, you will be asked to create a PIN.
		</li>
        
        <li>
            <b>Card balance:</b>
            Details of your Card available balance and transaction history are available at the DP e-wallet. You are responsible to check your balance before making any transaction. Transaction history remain on the website for a period of one year or until the expiry of the Card, whichever comes first. 
		</li>
		
        <li>
            <b>Expiry:</b>
            The Card is valid until the date printed on the front of the Card. Any remaining value on the Card is forfeited and becomes unusable automatically when the Card expires.
		</li>
        
        <li>
            <b>Using the Card:</b><br>
            (a)	The Card can be used for purchases of goods and services, where UnionPay prepaid cards are accepted, including at online merchants who accept UnionPay Online Payment (UPOP). However, some merchants may impose their own conditions on the use of the Card, such as minimum payment amounts.<br><br>

            (b)	You can use your virtual Card for payment of online transactions at selected online merchants and for payments through DP e-Wallet by linking the virtual Card to your DP e-Wallet.<br><br>

            (c)	The Card cannot be used to redeem or withdraw cash at any ATMs or over the counter at financial institutions.<br><br>

            (d)	Each time you use the Card, you authorise us to deduct the amount of the transaction from the balance of funds on the Card. The available balance cannot be exceeded. Where a purchase exceeds the available balance, the excess must be paid using other payment method. The Card cannot be used for direct debit, recurring or instalment payments.<br><br>

            (e)	You cannot stop payment on any transaction made with the Card.<br><br>

            (f)	Transaction using the Card may be declined at some merchants. We are not liable in any way when transaction is declined or a transaction otherwise does not proceed, regardless of reason.<br><br>

            (g)	All transactions in foreign currency will be converted into Australian dollars. Transactions will either be converted directly into Australian dollars or will be first converted from the currency in which the transaction was made into US dollars and then converted to Australian dollars at a rate determined by UnionPay International. A fee of [2%] of the total amount of each transaction will also be deducted from your Card for any transaction in foreign currency or any transaction in any currency (including AUD) that is billed by a merchant outside of Australia.<br><br>

            (h)	You are responsible for all transactions on the Card, except where there has been fraud or negligence by our staff or agents. If you notice any error relating to the Card, you should notify us immediately at +61 2 9267 7711 or email us at <a target='_blank' href="mailto:dpau@dynamicg.com">dpau@dynamicg.com</a>.
		</li>
		
        <li>
            <b>Specific terms and conditions applicable to the use of DP e-Wallet</b><br>
            (a)	You may choose to use your Card through our digital wallet application, DP e-Wallet Application ("App"). You can download the App from App Store or Google Play onto your smartphone ("Device"). More information about DP e-Wallet is available on our website: <a target='_blank' href="https://www.dynamic-payment.com/au/prepaid/activate.html">https://www.dynamic-payment.com/au/prepaid/activate.html</a><br><br>
            
            (b)	By downloading the App and registering an e-wallet within the App ("e-Wallet"), you are agreeing to these specific terms and conditions in addition to all other Terms in this agreement.<br><br>

            (c)	You shall be responsible for maintaining the confidentiality of your username and password to your e-Wallet.<br><br>

            (d)	By linking a Card to your e-Wallet, you are authorising payment from your Card through the App. You may link more than one Card to your e-Wallet at any point of time subject to an annual maximum limit of AUD 10,000.00<br><br>

            (e)	Payment through your e-Wallet is subject to a maximum limit of AUD 1,000.00 per transaction per card.<br><br>

            (f)	Payment through your e-Wallet is completed by either you scanning the QR Code displayed by the merchant with the App on your Device or by the merchant scanning the QR Code displayed on your device and you entering your PIN to confirm the transaction. Once the transaction is confirmed it cannot be stopped or cancelled.<br><br>

            (g)	DP e-Wallet payment method may not be accepted at all places where the Card is accepted, in which case, you can still use the physical Card.<br><br>

            (h)	By using the e-Wallet, you acknowledge and agree that DP is not in any way providing any banking service and DP is not acting as a trustee or fiduciary in respect of your funds.<br><br>

            (i)	You agree that by providing telephone number, email address and other contact information to us in registering your e-Wallet, you consent to us contacting you using such information. You also consent to receiving text messages and emails from DP or its authorised agents in connection to the App or Card including notification and promotional materials.<br>
		</li>
        <li>
            <b>Lost or stolen card:</b>
            You must take reasonable care to safeguard your Card against loss, theft or misuse. A Card is anonymous and is similar to cash. We are not able to replace the Card or refund the balance in the event the Card is misused, lost, stolen, or damaged. 
        </li>
        <li>
            <b>Complaint:</b>
            If you have a complaint about the Card, or service, please call Dynamic Payment on +61 2 9267 7711. If you have any enquiries or complaints in relation to your Card, you should, in the first instance, contact Dynamic Payment. If you feel that your complaint has not been addressed to your satisfaction, you may escalate the matter to UnionPay International. The contact details for UnionPay International are +61 2 9250 8888
        </li>
        <li>
            <b>Suspension:</b>
            For security reasons, including where we have reason to suspect fraud or other illegal activity, we may suspend the Card, prevent a transaction, or stop the use of the Card.
        </li>
        <li>
            <b>Dispute on transaction:</b>
            Any dispute on a transaction is a matter solely between you and the merchant and must be handled by you and the merchant directly. If the merchant agrees to give you a refund and if we receive the refund amount, we will credit it to your Card accordingly. If the Card expires or is revoked before you have spent any funds resulting from a refund, then you will have no access to those funds.
        </li>
        <li>
            <b>Disclosure:</b>
            We may disclose information about the Card or transactions made with the Card to third party whenever allowed by law, required by law or where it is necessary to operate the Card and process transactions.
        </li>
        <li>
            <b>Amendment:</b>
            We reserve the right to change these Terms at any time by posting a notice on our website <a target='_blank' href="https://www.dynamic-payment.com">https://www.dynamic-payment.com</a> at least 60 days prior to the effective day of the change.
        </li>
        <li>
            <b>Our rights to assign:</b>
            We can assign any or all of our rights under these terms and conditions to any other party.
        </li>
        <li>
            <b>Limits of our liability:</b>
            Our liability to you in connection with the Card, if any, will be limited to the unused balance that remains on your Card. Under no circumstances will we be liable for any indirect or consequential losses, even if advised of the possibility of such losses.
        </li>
        <li>
            <b>Partial invalidity:</b>
            If any of these terms are found to be unenforceable, it shall not affect the validity of the rest of these terms.
        </li>
        <li>
            <b>Governing law and jurisdiction:</b>
            The laws of Australia apply to these Terms and you irrevocably submit to and accept the exclusive jurisdiction of any of the Courts of Australia.
        </li>
      </ul>
	  
      <h3>Terms and Conditions – Virtual and Reloadable Stored Value Virtual Cards</h3>
	  
      <p>The Dynamic Payment Pty Ltd (ACN 114 195 050) ("DP") non-cash payment facility in the form of reloadable and virtual pre-paid cards is issued by DP and distributed by a Referrer ("the Virtual Card"). By purchasing or using the Virtual Card you agree to the terms and conditions stated herein ("Terms"). In these Terms, 'you' means the Virtual Card purchaser or user and 'we', 'our' or 'us' means DP. These Terms should be read in conjunction with the PDS and TMD for the Virtual Card.</p>
        <ul class="f_lis">
            <li>
                <b>General:</b>
                The Virtual Card must be used in accordance with the terms set out in the Product Disclosure Statement ("PDS") and these Terms. 
            </li>
            <li>
                <b>Stored Value:</b>
                The Virtual Card is a prepaid card loaded with an amount of funds determined by you when you purchase it through an available Application ("the Referrer's App"). It can be reloaded with additional value. A maximum value of AUD $4,999.00 can be held on the Virtual Card at any one time.
            </li>
            <li>
                <b>The Referrer's App and DP Card Management Platform</b><br>
                (a)	You can use your Virtual Card through the Referrer's App. You can download the Referrer's App from Apple App Store or Google Play Store onto your smartphone ("Device").<br><br>
                
                (b)	You shall be responsible for maintaining the confidentiality of your username and password to your Referrer's App account.<br><br>
                
                (c)	There is a Card Management Platform which can be accessed within the Referrer's App to manage your Virtual Card account ("the Card Management Platform"). Please see section 15 of the PDS for further details of the Card Management Platform.<br><br>
                
                (d)	Payment through the Virtual Card on the Referrer's App is subject to the balance available on the Virtual Card and its maximum limit of AUD $4,999.00.<br><br>
                
                (e)	Payment through the Referrer's App is completed by either you scanning the QR Code displayed by the merchant with the Referrer's App on your Device or by the merchant scanning the QR Code displayed on your Device and you entering your PIN to confirm the transaction. Once the transaction is confirmed it cannot be stopped or cancelled.<br><br>
                
                (f)	By using the Referrer's App to access the Virtual Card, you acknowledge and agree that DP is not in any way providing any banking service and DP is not acting as a trustee or fiduciary in respect of your funds.<br><br>
                
                (g)	You agree that by providing telephone number, email address and other contact information to us in registering your Virtual Card, you consent to us contacting you using such information. You also consent to receiving text messages and emails from DP and any authroised agents in connection to the Referrer's App, Card Management Platform or Virtual Card including notification and promotional materials. If you do not want to receive promotional material you can request to 'opt-out' of receiving these notifications.<br><br> 
                
                (h)	We will not sell or rent details we collect from you to unrelated third parties. In accordance with our Privacy Policy available at: 
                <a target='_blank' href="https://www.dynamic-payment.com/en/privacy_statement.html">https://www.dynamic-payment.com/en/privacy_statement.html</a><br><br>
                
                We will only share personal information with overseas parties where necessary to provide our services.
            </li>
            <li>
                <b>Activation:</b>
                You need to activate the Virtual Card you purchased before it can be used. You will need to access the Card Management Platform located on the Referrer's App and follow the instructions to activate the Virtual Card. To activate your Virtual Card you must provide DP with either:<br><br>
                •<span>&nbsp;&nbsp;</span>A valid Australian Driver's Licence; or<br>
                •<span>&nbsp;&nbsp;</span>A valid Passport; or<br>
                •<span>&nbsp;&nbsp;</span>National ID<br><br>
                You must also provide DP with an email or mobile phone number to confirm your identity.
            </li>
            <li>
                <b>Age Limit:</b>
                You must be at least 18 years of age to purchase the Virtual Card.
            </li>
            <li>
                <b>PIN:</b>
                When you activate your Virtual Card in the Card Management Platform, you will be asked to create a PIN. This PIN will be required to make payments for goods and services when using the Virtual Card. You must ensure that your PIN is kept secure at all times.
            </li>
            <li>
                <b>Adding Funds:</b>
                You can add funds to the Virtual Card through the Card Management Platform located on the Referrer's App. You can only add funds if the Virtual Card's available balance is less than AUD $4,999.00. You cannot add funds to that the Virtual Card if the balance of the Virtual Card will exceed AUD $4,999.00.
            </li>
            <li>
                <b>Virtual Card Balance:</b>
                Details of your Virtual Card's available balance and transaction history are available on the Card Management Platform. You are responsible to check your available balance before making any transaction. Your transaction history remains on the Card Management Platform until the expiry of the Virtual Card.
            </li>
            <li>
                <b>Expiry:</b>
                The Virtual Card is valid until the date shown in the Card Management Platform. Prior to the expiry date of the Virtual Card, DP will allow you to either:<br>
                a)	Deactivate the Virtual Card; or<br><br>
                b)	Extend the expiry date of the Virtual Card. Any remaining value on the Virtual Card when the Virtual Card expires will either:<br><br>
                a)	Be forfeited; or<br><br>
                b)	Be transferred to a nominated Australian bank account.<br><br>

                A transfer of the remaining balance to an Australian bank account must be requested by you within the month the Virtual Card is to expire. If you elect to transfer the remaining balance to your nominated bank account, the remaining balance of the Virtual Card must be more than the administration fee that DP charges for the transfer being AUD $10.00 plus any surcharge your nominated Australian bank charges. If your Virtual Card does not have sufficient funds remaining, you cannot transfer the remaining balance and the funds will be forfeited. Further details can be found in sections 16 and 17 of the PDS. 
            </li>
            <li>
                <b>Using the Virtual Card:</b><br>
                (a)	The Virtual Card can be used for purchase of goods and services, where UnionPay prepaid cards are accepted, including at online merchants who accept UPOP. However, some merchants may impose their own conditions on the use of the Virtual Card, such as minimum payment amounts.<br><br>
                (b)	You can use your Virtual Card for payment of online transactions at selected online merchants where UnionPay Online Payment ("UPOP") is accepted and for payments through the Referrer's App. DP is not liable if payment via the Referrer's App is not accepted at places where UPOP is accepted.<br><br>
                (c)	The Virtual Card cannot be used to redeem or withdraw cash at any ATMs or over the counter at financial institutions.<br><br>
                (d)	Each time you use the Virtual Card, you authorise us to deduct the amount of the transaction from the balance of funds on the Virtual Card. The available balance cannot be exceeded. Where a purchase exceeds the available balance, the excess must be paid using another payment method. The Virtual Card cannot be used for direct debit, recurring or instalment payments.<br><br> 
                (e)	You cannot stop payment on any transaction made with the Virtual Card.<br><br>
                (f)	Transaction using the Virtual Card may be declined at some merchants. We are not liable in any way when a transaction is declined or a transaction otherwise does not proceed, regardless of reason.<br><br> 
                (g)	You are responsible for all transactions on the Virtual Card, except where there has been fraud or negligence by our staff or agents. If you notice any error relating to the Virtual Card, you should notify us immediately at +61 2 9267 7711 or email us at <a target="_blank" href="mailto:dpau@dynamicg.com">dpau@dynamicg.com</a>. Please refer to our PDS for full details of your obligations as a cardholder and transaction disputes.
            </li>
            <li>
                <b>Foreign Transaction:</b>
                The Virtual Card can be used to make foreign transactions. All transactions in foreign currency will be converted into Australian dollars. Transactions will either be converted directly into Australian dollars or will be first converted from the currency in which the transaction was made into US dollars and then converted to Australian dollars at a rate determined by UnionPay International. A fee of 2% of the total amount of each transaction will also be deducted from your Virtual Card for any transaction in foreign currency or any transaction in any currency (including AUD) that is billed by a merchant outside of Australia. Please see section 18 of the PDS for a details of all fees and charges associated with using the Virtual Card. 
            </li>
            <li>
                <b>Your Liability:</b>
                You must take reasonable care to safeguard your Virtual Card against loss, theft or misuse. We are not liable to refund the whole or any part of the balance in the event that you caused or partly caused on the balance of probabilities the Virtual Card to be misused, lost or stolen. Please see section 11 of the PDS which sets out your liability for transactions using the Virtual Card.
            </li>
            <li>
                <b>Complaint:</b>
                If you have a complaint about the Virtual Card, or service, please call DP on +61 2 9267 7711. If you have any enquiries or complaints in relation to your Virtual Card, you should, in the first instance, contact DP. If you feel that your complaint has not been addressed to your satisfaction, you may escalate the matter to the Australian Financial Complaints Authority (<b>AFCA</b>). The contact details for AFCA are:<br><br>
                
                Mail: GPO Box 3, Melbourne, Victoria, 3001<br>
                Telephone No: 1800 931 678<br>
                Email: info@afca.org.au<br>
                Website: <a target='_blank' href="https://www.afca.org.au/">https://www.afca.org.au/</a><br>
            </li>
            <li>
                <b>Suspension:</b>
                For security reasons, including where we have reason to suspect fraud or other illegal activity, we may suspend the Virtual Card, prevent a transaction, freeze the funds available on the Virtual Card or terminate the Virtual Card. 
            </li>
            <li>
                <b>Dispute on Transaction:</b>
                Any dispute on a transaction relating to the quality of goods or services is a matter solely between you and the merchant and must be handled by you and the merchant directly. If the merchant agrees to give you a refund and if we receive the refund amount, we will credit it to your Virtual Card accordingly. If the Virtual Card expires or is revoked before you have spent any funds resulting from a refund, then you will have no access to those funds. DP is only liable for a transaction dispute if we or our agent's actions, or inactions result in a transaction occurring that should not have. Please see section 10 of the PDS for further details.
            </li>
            <li>
                <b>Disclosure:</b>
                We may disclose information about the Virtual Card or transactions made with the Virtual Card to a third party whenever allowed by law, required by law or where it is necessary to operate the Virtual Card and process transactions.
            </li>
            <li>
                <b>Amendment:</b>
                We reserve the right to change these Terms at any time by posting a notice on our website <a target='_blank' href="https://www.dynamic-payment.com">https://www.dynamic-payment.com</a> at least 60 days prior to the effective day of the change.
            </li>
            <li>
                <b>Our Rights to Assign:</b>
                We can assign any or all of our rights under these terms and conditions to any other party.
            </li>
            <li>
                <b>Your Rights to Assign:</b>
                You cannot assign any of your rights or obligations under these terms and conditions and any other terms relating to the Virtual Card to any other party.
            </li>
            <li>
                <b>Limits of Our Liability:</b>
                Our liability to you in connection with the Virtual Card, if any, will be limited to the unused balance that remains on your Virtual Card. Under no circumstances will we be liable for any indirect or consequential losses, even if advised of the possibility of such losses, where you have contributed to the loss.
            </li>
            <li>
                <b>Partial Invalidity:</b>
                If any of these terms are found to be unenforceable, it shall not affect the validity of the rest of these terms.
            </li>
            <li>
                <b>Governing Law and Jurisdiction:</b>
                The laws of Australia apply to these Terms and you irrevocably submit to and accept the exclusive jurisdiction of any of the Courts of Australia.
            </li>
            
        </ul>

        <h3>e-Wallet Terms of Use</h3>
        
        <p>These Terms of Use apply to the e-Wallet Mobile Application owned by Dynamic Payment Pty Ltd ('DP'). Please read these Terms of Use carefully before using the DP e-Wallet Mobile Application ('App').</p>
        
        <p>By using the App you are deemed to have read and accepted these Terms of Use. Please refrain from using the App if you do not agree to all or any of these Terms of Use. DP shall not be liable for payment of any costs or expenses incurred as a result of downloading and using the App, including any operator network and/or roaming charges.</p>
        
        <ul class="f_lis">
            <li>
                <b>Intellectual Property</b><br>
                a)	The contents of the DP App are intended for your personal non-commercial use only. Graphics and images on the App are protected by copyright and may not be reproduced, translated or appropriated in any manner without our written permission. Modification of any of the materials or use of the materials for any other purpose will be a violation of DP's copyright and other intellectual property rights and the copyright and intellectual property rights of the respective owners.<br><br>

                b)	Nothing in the Terms of Use herein gives you any right, title or interest in any intellectual property rights relating to the App, including any brands, trademarks (whether registered or otherwise), music or soundbites and/or images and designs. You may use the material on the App for the purpose of accessing DP e-Wallet services only. You must not duplicate, copy, reproduce, reverse-engineer, distribute, transmit, adapt, republish, display, or use the App or any of its contents, in any form or manner, except to use it for the said purpose.
            </li>
            <li>
                <b>Use of the App</b><br>
                a)	You agree to use the App in accordance with these Terms of Use and in a lawful manner and for its intended purpose. You agree to be responsible for all matters arising from your use of the App. Further, you agree not to use the App in any manner which may breach any applicable law or regulations or causes or which may cause an infringement of any third party rights, not to post, transmit or disseminate any information on or via the App which may be harmful, obscene, defamatory or illegal or create liability on our part, not to interfere or attempt to interfere with the operation or functionality of the App and not to obtain or attempt to obtain unauthorised access, via any means, to any of DP's systems.<br><br>

                b)	If we have any reason to believe that you are in breach, or will be in breach, of any of these Terms of Use, we reserve the right to terminate your access to the App immediately.
            </li>
            <li>
                <b>Other Sites</b><br>
                The App may provide links to other sites and vice versa merely for your convenience and information. We shall neither be responsible for the content and availability of such other applications that may be operated and controlled by third parties and by our various branch offices and affiliates, nor for the information, products or services contained on or accessible through those mobile applications. Your access and use of such mobile applications remain solely at your own risk. DP will not be liable for any direct, indirect, consequential losses and/or damages of any kind arising out of your access to such applications.
            </li>
            <li>
                <b>No Warranties</b><br>
                We provide no warranty, whether expressly or implied, of any kind, including but not limited to any implied warranties or implied terms of satisfactory quality, fitness for a particular purpose or non-infringement. To the extent permitted by law, all such implied terms and warranties are hereby excluded.
            </li>
            <li>
                <b>Disclaimer</b><br>
                We do not warrant that the functions or operation of the App will be uninterrupted or error free, or that defects will be corrected or that the App or the server that makes it available is free of any virus or other harmful elements. While we use reasonable efforts to include accurate and up-to-date information on the App, we do not warrant or make any representations regarding the correctness, accuracy, reliability or otherwise of the materials in the App or the results of their use.
            </li>
            <li>
                <b>Liability</b><br>
                (a)	To the extent permitted by law, we do not accept any liability or responsibility whatsoever if the App is not available, complete or error-free over any period or at any particular time.<br><br>

                (b)	By using the App you agree that DP will not be liable under any circumstances, including negligence, for any direct, indirect or consequential loss arising from your use of the information and material contained in the App or from your access to any of the linked sites. We are not liable nor responsible for any material provided by third parties with their own respective copyright and shall not under any circumstances, be liable for any loss, damages or injury arising from these materials.<br><br>

                (c)	The information contained in the App is for informational purposes only and is provided to you on an "as-is" basis. We do not guarantee the accuracy, timeliness, reliability, authenticity of completeness of any of the information contained on the App. We are not liable for any information or services which may appear on any linked mobile applications or websites.
            </li>
            <li>
                <b>Security</b><br>
                The App may require you to choose and input a personal identification number ('PIN'). You are responsible for keeping that PIN safe and secure so as to prevent any unauthorised use of and access to your e-Wallet. You will be responsible for all use of your e-Wallet through the App and all purchases made through your e-Wallet notwithstanding any unauthorised access.
            </li>
            <li>
                <b>Changes and Modifications</b><br>
                (a)	We reserve the right at our absolute discretion and without liability to change, modify, alter, adapt, add or remove any of the terms and conditions contained herein and/or change, suspend or discontinue any aspect of the App. The revised term shall take effect from the time it is posted.<br><br>

                (b)	We are not required to give you any advanced notice prior to incorporation of any of the above changes and/or modifications into the App.
            </li>
            <li>
                <b>Privacy</b><br>
                (a)	To use the App, you are required to provide certain personal information. We collect, process and use personal information in accordance with these Terms of Use and our Privacy Policy, which can be found at<br>
                <a target='_blank' href="https://www.dynamic-payment.com/en/privacy_statement.html">https://www.dynamic-payment.com/en/privacy_statement.html</a>.<br><br>

                (b)	By accessing the App and the use of services offered on the App, you are deemed to have accepted our Privacy Policy and the terms herein.<br><br>

                (c)	In addition to the purposes listed in our Privacy Policy, we may use any information you provide us to:<br>
                •<span>&nbsp;&nbsp;</span>	provide you e-Wallet service through the App (including by disclosing personal information to our authorised third-party contractors);<br>
                •<span>&nbsp;&nbsp;</span>contact you with information regarding the App;<br>
                •<span>&nbsp;&nbsp;</span>monitor your use of the App;<br>
                •<span>&nbsp;&nbsp;</span>help resolve and manage issues with the App;<br>
                •<span>&nbsp;&nbsp;</span>report on App usage to help us improve the App;<br>
                •<span>&nbsp;&nbsp;</span>conduct market research to better understand your needs in relation to the App and the DP e-Wallet service;<br>
                •<span>&nbsp;&nbsp;</span>personalise any of our services and products for you and to deliver targeted advertisements and offers from us and third parties;<br>
                •<span>&nbsp;&nbsp;</span>identify offers, promotions, new products, improvements, or other information we think you might find interesting, tell you about them, and provide them to you whether through the App or other channels such as e-mail.<br>

            </li>
            <li>
                <b>Applicable Law and Jurisdiction</b><br>
                These Terms of Use and this Mobile App's content shall be governed and construed in accordance with Australia laws, and the courts of Australia shall have exclusive jurisdiction to adjudicate any dispute which may arise in relation thereto.
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</section>
<?php
  include("footer.php");
?>
<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>
</body>
</html>
