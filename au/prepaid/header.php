<header id="fh5co-header">
  <div class="fluid-container">
    <nav class="navbar gtco-nav navbar-default">
      <div class="navbar-header"> 
        <!-- Mobile Toggle Menu Button --> 
        <!--<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar"><i></i></a> //-->
        <a class="navbar-brand" href="../../en/index.html"><img alt="Dynamic Payment" name="Dynamic Payment" src="../../../images/logo.svg"/></a>
        <div class="top_payicon"> <img alt="UnionPay1" name="UnionPay1" src="../../images/top/UnionPay_logo1.png"> <img alt="WeChat" name="WeChat" src="../../../images/top/WeChat_s.png"> <img alt="Alipay" name="Alipay" src="../../../images/top/Alipay_s.png"> <img alt="UnionPay2" name="UnionPay2" src="../../../images/top/UnionPay_s.png"> </div>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        
      </div>
    </nav>
  </div>
</header>