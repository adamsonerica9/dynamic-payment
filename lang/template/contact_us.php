<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{templateMeta}}
</head>

<body>
    {{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t contact_bg">{{contactUs}}</div>
</div>
</section>

<section id="fh5co-contact">
<div class="container">
    <div class="page_t">
				<h3><img class="contact_flag" src="../images/countries/Australia.jpg" alt="Australia National Flag">{{australia}}</h3>

        <p><strong>{{address}}</strong>{{australiaAddressDetail}}<br />

		<strong>{{tel}}</strong>{{australiaTelsDetail}}<br />

		<strong>{{fax}}</strong>{{australiaFaxDetail}}<br />

		<strong>{{email}}</strong>	<a href="mailto:{{australiaEmailDetail}}">{{australiaEmailDetail}}</a></p>

		<div class="divider"></div>

		

		<h3><img class="contact_flag" src="../images/countries/Cambodia.jpg" alt="Cambodia National Flag">{{cambodia}}</h3>

        <p><strong>{{address}}</strong>{{cambodiaAddressDetail}}<br />

		<strong>{{tel}}</strong>{{cambodiaTelsDetail}}<br />

		<strong>{{fax}}</strong>{{cambodiaFaxDetail}}<br />

		<strong>{{email}}</strong>	<a href="mailto:{{cambodiaEmailDetail}}">{{cambodiaEmailDetail}}</a></p>

        <div class="divider"></div>

        

		<h3><img class="contact_flag" src="../images/countries/Fiji.jpg" alt="Fiji National Flag">{{fiji}}</h3>

        <p><strong>{{address}}</strong>{{fijiAddressDetail}}<br />

		<strong>{{tel}}</strong>{{fijiTelsDetail}}<br />

		<strong>{{fax}}</strong>{{fijiFaxDetail}}<br />

		<strong>{{email}}</strong>	<a href="mailto:{{fijiEmailDetail}}">{{fijiEmailDetail}}</a></p>

        <div class="divider"></div>

        	

		<h3><img class="contact_flag" src="../images/countries/HK.jpg" alt="HK Flag">{{hk}}</h3>

        <p><strong>{{address}}</strong>{{hkAddressDetail}}<br />

		<strong>{{tel}}</strong>{{hkTelsDetail}}<br />

		<strong>{{fax}}</strong>{{hkFaxDetail}}<br />

		<strong>{{email}}</strong>	<a href="mailto:{{hkEmailDetail}}">{{hkEmailDetail}}</a></p>

        <div class="divider"></div>

		

        <h3><img class="contact_flag" src="../images/countries/New_Zealand.jpg" alt="New Zealand National Flag">{{newZealand}}</h3>

        <p><strong>{{address}}</strong>{{newZealandAddressDetail}}<br />

		<strong>{{tel}}</strong>{{newZealandTelsDetail}}<br />

		<strong>{{fax}}</strong>{{newZealandFaxDetail}}<br />

		<strong>{{email}}</strong>	<a href="mailto:{{newZealandEmailDetail}}">{{newZealandEmailDetail}}</a></p>

        <div class="divider"></div>

		

		<h3><img class="contact_flag" src="../images/countries/Papua.jpg" alt="Papua National Flag">Papua New Guinea (coming soon)</h3>

		<p>
	
		<strong>{{tel}}</strong>	+61 2 9267 7711<br />

		<strong>{{fax}}</strong>	+61 2 9267 7971<br />

		<strong>{{email}}</strong>	<a href="mailto:{{papuaNewGuineaEmailDetail}}">{{papuaNewGuineaEmailDetail}}</a></p>

        <div class="divider"></div>

        

		<h3><img class="contact_flag" src="../images/countries/Philippines.jpg" alt="Philippines National Flag">{{philippines}}</h3>

		<p>
		<strong>{{address}}</strong>{{philippinesAddressDetail}}<br />
		
		<strong>{{tel}}</strong>{{philippinesTelsDetail}}<br />
		
        <strong>{{fax}}</strong>{{philippinesFaxDetail}}<br />
		
		<strong>{{email}}</strong>	<a href="mailto:{{philippinesEmailDetail}}">{{philippinesEmailDetail}}</a></p>

        <div class="divider"></div>

		

		<h3><img class="contact_flag" src="../images/countries/Solomon_Islands.jpg" alt="Solomon Islands National Flag">{{solomonIslands}}</h3>

		<p>
		
		<strong>{{tel}}</strong>{{solomonIslandsTelsDetail}}<br />

		<strong>{{fax}}</strong>{{solomonIslandsFaxDetail}}<br />

		<strong>{{email}}</strong>	<a href="mailto:{{solomonIslandsEmailDetail}}">{{solomonIslandsEmailDetail}}</a></p>

        <div class="divider"></div>

		

        <h3><img class="contact_flag" src="../images/countries/Thailand.jpg" alt="Thailand National Flag">{{thailand}}</h3>

        <p><strong>{{address}}</strong>{{thailandAddressDetail}}<br />

		<strong>{{tel}}</strong>{{thailandTelsDetail}}<br />

		<strong>{{fax}}</strong>{{thailandFaxDetail}}<br />

		<strong>{{email}}</strong>	<a href="mailto:{{thailandEmailDetail}}">{{thailandEmailDetail}}</a></p>

   		<div class="divider"></div>

   		

   		<h3><img class="contact_flag" src="../images/countries/Tonga.jpg" alt="Tonga National Flag">{{tonga}}</h3>

        <p><strong>{{address}}</strong>{{tongaAddressDetail}}<br />

		<strong>{{tel}}</strong>{{tongaTelsDetail}}<br />

		<strong>{{fax}}</strong>{{tongaFaxDetail}}<br />

		<strong>{{email}}</strong>	<a href="mailto:tongaEmailDetail">{{tongaEmailDetail}}</a></p>

  		<div class="divider"></div>

  		

   		<h3><img class="contact_flag" src="../images/countries/Vanuatu.jpg" alt="Vanuatu National Flag">Vanuatu</h3>

        <p><strong>{{address}}</strong> Suite 6, 18 Savou Street, Westfield Subdivision, Nadi, Fiji<br />

		<strong>{{tel}}</strong>	+679 672 1085<br />

		<strong>{{fax}}</strong>	+679 672 1084<br />

		<strong>{{email}}</strong>	<a href="mailto:dppc@dynamicg.com">dppc@dynamicg.com</a></p>
		
		<div class="divider"></div>
	<script type="text/javascript">
	function displayResult() {
		var x = document.getElementById("mySelect");
		t = x.value
		
		var y = document.getElementById("myTitle");
		m = y.value
		
		var z = document.getElementById("myMessage");
		n = z.value
		
		var a = document.getElementById("myName");
		o = a.value
		
		var b = document.getElementById("myCompany");
		p = b.value
		
		var c = document.getElementById("myContactNo");
		q = c.value
		
		var d = document.getElementById("myEmail");
		r = d.value
		
		document.getElementById("myEmailList").href = "mailto:"+t+"?subject="+m+"&body="+n+"%0A%0AName:"+o+"%0A%0ACompany:"+p+"%0A%0AContact No:"+q+"%0A%0AEmail {{address}}"+r;
	}
	</script>

		<h3>{{emailCountactUs}}</h3>
		
<FORM>
	<div>{{optionSelect}}</div>
	<select name="inputbox1" id="mySelect">
		<option value="dpau@dynamicg.com">{{australia}}</option>
		<option value="dpkh@dynamicg.com">{{cambodia}}</option>
		<option value="dppc@dynamicg.com">{{fiji}}</option>
		<option value="dphk@dynamicg.com">{{hk}}</option>
		<option value="dpnz@dynamicg.com">{{newZealand}}</option>
		<option value="dpau@dynamicg.com">{{papuaNewGuinea}}</option>
		<option value="dpph@dynamicg.com">{{philippines}}</option>
		<option value="dphk@dynamicg.com">{{solomonIslands}}</option>
		<option value="dpth@dynamicg.com">{{thailand}}</option>
		<option value="dppc@dynamicg.com">{{tonga}}</option>
		<option value="dppc@dynamicg.com">{{vanuatu}}</option>
	</select>
	<br/><br/>
    <div>{{subjectTitle}}</div>
	<INPUT TYPE="text" NAME="inputbox2" id="myTitle" SIZE="50" style="width:600px"/>
	<br/><br/>
	
	<div>{{myName}}</div>
	<INPUT TYPE="text" NAME="inputbox4" id="myName" SIZE="50" style="width:600px"/>
	<br/><br/>
	
	<div>{{myCompany}}</div>
	<INPUT TYPE="text" NAME="inputbox5" id="myCompany" SIZE="100" style="width:600px"/>
	<br/><br/>
	
	<div>{{contactNo}}</div>
	<INPUT TYPE="text" NAME="inputbox6" id="myContactNo" SIZE="20" style="width:600px"/>
	<br/><br/>
	
	<div>{{email}}</div>
	<INPUT TYPE="text" NAME="inputbox7" id="myEmail" SIZE="50" style="width:600px"/>
	<br/><br/>

    <div>{{message}}</div>
    <textarea name="inputbox3" id="myMessage" MAXLENGTH="500" style="width:600px" rows="5" ></textarea>

    <br/><br/>
	<button type="button" onclick="displayResult()">{{confirmLocation}}</button>
	<a id="myEmailList" href="mailto:">{{sendemail}}</a><BR>
</FORM>

    </div>
    <div class="clearfix"></div>
	
</div>

</section>

    {{templateFooter}}

</body>
</html>
