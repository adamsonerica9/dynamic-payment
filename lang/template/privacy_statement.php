<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{{templateMeta}}
</head>

<body>
	{{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t aboutus_bg">{{privacyPolicyHeader}}</div>
</div>
</section>

<section id="fh5co-about">
<div class="container">
    <div class="page_t">
        <h3>{{overviewTitle1}}</h3>
        <p>
		{{overviewDesc1a}}<br /><br />

		{{overviewDesc1b}}
		</p>
		
        <h3>{{overviewTitle2}}</h3>
        <p>
		{{overviewDesc2}}
		</p>
		
        <h3>{{overviewTitle3}}</h3>
        <p>
		{{overviewDesc3}}
		</p>
		
        <h3>{{transactionDataTitle}}</h3>
        <p>
		{{transactionDataDesc}}
		</p>
		
        <h3>{{voluntaryInformationTitle}}</h3>
        <p>
		{{voluntaryInformationDesc}}
		</p>

        <h3>{{useOfCookiesTitle}}</h3>
        <p>
		{{useOfCookiesDesc1}}<br /><br />

		{{useOfCookiesDesc2}}
		</p>

        <h3>{{personalInformationTitle}}</h3>
        <p>
		{{personalInformationDesc1}}<br /><br />

        {{personalInformationDesc2}}
		<ul>
			<li>{{personalInformationDesc3}}</li>
			<li>{{personalInformationDesc4}}</li>
			<li>{{personalInformationDesc5}}</li>
		</ul>
		</p>
        <p>
		{{personalInformationDesc6}}<br /><br />
		
		{{personalInformationDesc7}}<br /><br />
		
		{{personalInformationDesc8}}
		</p>
		
		<h3>{{disclosureOverseasTitle}}</h3>
        <p>
		{{disclosureOverseasDesc1}}<br /><br />
		
		{{disclosureOverseasDesc2}}<br /><br />  

		{{disclosureOverseasDesc3}}<br /><br />

		{{disclosureOverseasDesc4}}<br /><br />

		{{disclosureOverseasDesc5}}<br /><br />
		
		{{disclosureOverseasDesc6}}<br /><br />
		
		{{disclosureOverseasDesc7}}
		</p>
		
        <h3>{{informationSecurityTitle}}</h3>
        <p>
        {{informationSecurityDesc1}}
		<ul>
			<li>{{informationSecurityDesc2}}</li>
			<li>{{informationSecurityDesc3}}</li>	
			<li>{{informationSecurityDesc4}}</li>
		</ul>
        </p>
		<p>
		{{informationSecurityDesc5}}
		</p>
		
        <h3>{{changePrivacyPolicyTitle}}</h3>
        <p>
		{{changePrivacyPolicyDesc}}
		</p>

        <h3>{{contactDetailsTitle}}</h3>
        <p>
		{{contactDetailsDesc1}}<br /><br />

		{{contactDetailsDesc2}}<br>
		
		{{contactDetailsDesc3}} <a href="http://www.oaic.gov.au" target="_blank">http://www.oaic.gov.au</a>. 
		</p>
		
    </div>
    <div class="clearfix"></div>
</div>

</section>

	{{templateFooter}}
</body>
</html>
