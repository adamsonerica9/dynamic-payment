<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{templateMeta}}
</head>

<body>
    {{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t aboutus_bg">{{faqHeader}}</div>
</div>
</section>

<section id="fh5co-about">
<div class="container">
    <div class="page_t">
        <h2>{{faqTitle}}</h2>
        <h3>{{faq1}}</h3>
        <p>{{faq1Answer}}</p>
        <div class="divider"></div>
        <h3>{{faq2}}</h3>
        <p>{{faq2Answer}}</p>
        <div class="divider"></div>
        <h3>{{faq3}}</h3>
        <p>{{faq3Answer}}</p>
        <div class="fagcard_con">
            <p>{{previousUnionPayLogo}}</p>
            <img src="../images/faq/old_UnionPay_logo.png" alt="{{previousUnionPayLogo}}">            
        </div>
        <div class="fagcard_con">
            <p>{{currentUnionPayLogo}}</p>
            <img src="../images/faq/UnionPay_logo1.png" alt="{{currentUnionPayLogo}}">
            
        </div>
        <div class="fagcard_con">
            <p>{{pureUnionCardLogo}}</p>
             <img src="../images/faq/PureUnionCard.jpg" alt="{{pureUnionCardLogo}}">
        </div>
        <div class="fagcard_con">
            <p>{{dualBrandUnionPayLogo}}</p>
            <img src="../images/faq/DurlbrandUnionPaycard_1.jpg" alt="{{dualBrandUnionPayLogo}}">
            <img src="../images/faq/DurlbrandUnionPaycard_2.jpg" alt="{{dualBrandUnionPayLogo}}">
        </div>
        
        <div class="divider"></div>
        
        <h3>{{faq4}}</h3>
        <p>{{faq4Answer}}</p>
        <div class="fagcard_con">
            <p>{{debitCardSampleLogo}}</p>
            <img src="../images/faq/DebitCardSample.jpg" alt="{{debitCardSampleLogo}}">
        </div>
        <div class="fagcard_con">
            <p>{{creditCardSampleLogo}}</p>
            <img src="../images/faq/CreditCardSample.jpg" alt="{{creditCardSampleLogo}}">
        </div>
        
        <div class="divider"></div>
        
        <h3>{{faq5}}</h3>
        <p>{{faq5Answer1}}<br /><br />{{faq5Answer2}}</p>
        
        <div class="divider"></div>
        
        <h3>{{faq6}}</h3>
        <p>{{faq6Answer}}</p>
        
        <div class="divider"></div>
        
        <h3>{{faq7}}</h3>
        <p>{{faq7Answer1}}<br /><br />
		{{faq7Answer2}}<br /><br />
		{{faq7Answer3}}<br />
		<a href="{{faq7Answer4}}" target="_blank">{{faq7Answer4}}</a></p>
        
        <div class="divider"></div>
        
        <h3>{{faq8}}</h3>
        <p>{{faq8Answer}}</p>
        
        <div class="divider"></div>
        
        <h3>{{faq9}}</h3>
        <p>{{faq9Answer1}}<br />
        <ul class="s_lis">
        	<li>{{faq9Answer2}}</li>
        	<li>{{faq9Answer3}}</li>
            <li>{{faq9Answer4}}</li>
            <li>{{faq9Answer5}}</li>
            <li>{{faq9Answer6}}</li>
            <li>{{faq9Answer7}}</li>
            <li>{{faq9Answer8}}</li>
            <li>{{faq9Answer9}}</li>
            <li>{{faq9Answer10}}</li>
        </ul></p> 
    
        <div class="divider"></div>
        
        <h3>{{faq10}}</h3>
        <p>{{faq10Answer1}}<br /><br />
		{{faq10Answer2}}<br /><br />
		{{faq10Answer3}}<br /><br />
		{{faq10Answer4}}<br />
		<a href="{{faq10Answer5}}" target="_blank">{{faq10Answer5}}</a></p>    
    
        <div class="divider"></div>
    
        <h3>{{faq11}}</h3>
        <p>{{faq11Answer}}</p>
    
        <div class="divider"></div>
    
        <h3>{{faq12}}</h3>
        <p>{{faq12Answer}}</p>
    
        <div class="divider"></div>
    
        <h3>{{faq13}}</h3>
        <p>{{faq13Answer1}}<br /><br />
		{{faq13Answer2}}
		</p>
        
    </div>
    
    <div class="clearfix"></div>
</div>

</section>

    {{templateFooter}}
</body>
</html>
