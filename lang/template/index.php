<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{templateMeta}}
</head>

<body>
    {{templateHeader}}

<section id="fh5co-home">
<div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active">&nbsp;</li>
      <li data-target="#myCarousel" data-slide-to="1">&nbsp;</li>
      <li data-target="#myCarousel" data-slide-to="2">&nbsp;</li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <div class="La-banner La-bg1"></div>
        <div class="carousel-caption fade-in">
        	<i><img src="../images/icon_p1.svg" alt="Payment Method icon 1" /></i>
        	<h1>
            <!-- header json -->
            {{paymentMethod}}
          </h1>
            <p>{{paymentMethodDescription}}</p>
            <a href="payment_method.php" class="g_btn">{{readMore}}</a>
        </div>
      </div>

      <div class="item">
        <div class="La-banner La-bg2"></div>
        <div class="carousel-caption fade-in">
        	<i><img src="../images/icon_p2.svg" alt="Payment Method icon 2" /></i>
        	<h1>{{customizeSolution}}</h1>
            <p>{{customizeSolutionDescription}}</p>
            <a href="customize_solution.php" class="g_btn">{{readMore}}</a>
        </div>
      </div>
    
      <div class="item">
        <div class="La-banner La-bg3"></div>
        <div class="carousel-caption fade-in">
        	<i><img src="../images/icon_p3.svg" alt="Payment Method icon 3" /></i>
        	<h1>{{aboutUs}}</h1>
            <p>{{aboutUsDescription}}</p>
            <a href="about_us.php" class="g_btn">{{readMore}}</a>
        </div>
      </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">{{previous}}</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">{{next}}</span>
    </a>
  </div>
</div>
</section>

<a name="maincontent"></a>
<!--
<section id="fh5co-aboutus">
	<div class="full_wc">
    	<h2>Dynamic Payment</h2>
        <p>We are committed to exemplary Service to our employees, customers and communities. Driven by passion to innovate, we meet the needs of our customers. As payment technology experts, we bring our expertise and global perspective to Commerce.</p>       
        <a href="#">READ MORE</a>
    </div>
</section>
//-->
<section id="fh5co-services">
	<div class="p_row">
    	<div class="col-md-6 fea_txt left animate-box">
        	<h2 class="fea_hl">{{smartPOSTerminal}}</h2>
			<p>{{smartPOSTerminalDescription}}</p>
			<a href="pm_unionpay.php">{{readMore}}</a>
        </div>
        <div class="fea_img right col-md-6 animate-box"><img src="../images/f_pos.png"/ alt="POS Terminal" ></div>
    </div>
    <div class="p_row">
    	<div class="col-md-6 fea_txt right animate-box">
        	<h2 class="fea_hl">{{onlinePaymentSystem}}</h2>
			<p>{{onlinePaymentSystemDescription}}</p>
            <a href="payment_method.php">{{readMore}}</a>
        </div>
        <div class="fea_img left col-md-6 animate-box"><img src="../images/f_online.png" alt="Online Transaction" /></div>
    </div>
    <div class="p_row">
    	<div class="col-md-6 fea_txt left animate-box">
        	<h2 class="fea_hl">{{paymentSolutions}}</h2>
			<p>{{paymentSolutionsDescription}}</p>
            <a href="customize_solution.php">{{readMore}}</a>
        </div>
        <div class="fea_img right col-md-6 animate-box"><img src="../images/f_solution.png" alt="Customer Solution" /></div>
    </div>
</section>
<div class="clearfix"></div>

<section id="fh5co-features" class="features_r">
	<div class="full_wc">
    	<div class="hl_title"><h2>{{features}}</h2></div>
        <div class="col-md-4">
        	<i><img src="../images/f_icon_p3.svg" alt="Small Icon 3" /></i>
            <h4>{{ecommerce}}</h4>
            <p>{{ecommerceDescription}}</p>
        </div>
        <div class="col-md-4">
        	<i><img src="../images/f_icon_p2.svg" alt="Small Icon 2" /></i>
            <h4>{{easyPay}}</h4>
            <p>{{easyPayDescription}}</p>
        </div>
        <div class="col-md-4">
        	<i><img src="../images/f_icon_p1.svg" alt="Small Icon 1" /></i>
            <h4>{{customization}}</h4>
            <p>{{customizationDescription}}</p>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

    {{templateFooter}}
</body>
</html>
