<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{{templateMeta}}
</head>

<body>
	{{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t aboutus_bg">{{disputeResolution}}</div>
</div>
</section>

<section id="fh5co-about">
<div class="container">
    <div class="page_t">
		<h2>{{title1}}</h2>

        <h3>{{howTo}}</h3>
        <p>
		{{howToContent}}<br /><br />

		{{attention}}<br />
		{{company}}<br />
		{{address1}}<br />
		{{address2}}<br />
		{{address3}}<br />
		{{tel}} {{telContent}}<br />
		{{fax}} {{faxContent}}<br />
		{{email}} <a href="mailto:{{emailContent}}">{{emailContent}}</a>
		</p>
		
        <h3>{{whatHappensAfterComplainants}}</h3>
        <p>
			{{whatHappensAfterComplainantsContent}}
		</p>
		
        <h3>{{opportunityFor}}</h3>
        <p>
			{{opportunityForContent}}
		</p>
		
        <h3>{{whatHappensAfterDynamic}}</h3>
        <p>
			{{whatHappensAfterDynamicContent}}
		</p>
		
        <h3>{{complainantIsUnsatisfied}}</h3>
        <p>
			{{complainantIsUnsatisfiedContent}}
		</p>

        <h3>{{complaintIsClosed}}</h3>
        <p>
			{{complaintIsClosedContent}}
		</p>

        <h3>{{synamicPaymentMonitor}}</h3>
        <p>
			{{synamicPaymentMonitorContent}}
		</p>
		
		<h3>{{dynamicPaymentOutsource}}</h3>
        <p>
		{{dynamicPaymentOutsourceContent}}
		</p>
		
		<h2>{{title2}}</h2>
        <h3>{{introduction}}</h3>
        <p>
        	{{introductionContent}}
		</p>
		
        <h3>{{australianFinancial}}</h3>
        <p>
		{{australianFinancialContent}}<br /><br />

		{{australianFinancialAddress1}}<br />
		{{australianFinancialAddress2}}<br />
		{{tel}} {{australianFinancialTel}}<br />
		{{email}} <a href="mailto:{{australianFinancialEmail}}">{{australianFinancialEmail}}</a><br />
		{{website}} <a href="{{australianFinancialWebsite}}" target="_blank">{{australianFinancialWebsite}}</a><br />

		{{australianFinancialContentExtra}}<br /><br />

		<i>{{firstTier}}</i><br />
		{{firstTierContent}}<br /><br />

		<i>{{secondTier}}</i><br />
		{{secondTierContent}}
		</p>

        <h3>{{edrProcess}}</h3>
        <p>
			{{edrProcessContent}}
		</p>
		
    </div>
    <div class="clearfix"></div>
</div>

</section>


	{{templateFooter}}

</body>
</html>
