<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{templateMeta}}
</head>

<body>
    {{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t pm_bg">{{videosHeader}}</div>
</div>
</section>

<section id="fh5co-pm">
<div class="container">
    {{templateVideoMenu}}
    <div class="right_content page_t">
    	<a name="{{videomenuitem1target}}"></a>
    	<h3>{{video1Title}}</h3>
    	
        <iframe width="560" height="315" src="{{youTube1URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>{{youTube1}}</h4>  
	
        <iframe width="560" height="315" src="{{youTube2URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
        <h4>{{youTube2}}</h4>  
		
        <iframe width="560" height="315" src="{{youTube3URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
        <h4>{{youTube3}}</h4> 
	
        <iframe width="560" height="315" src="{{youTube4URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
        <h4>{{youTube4}}</h4> 

        <iframe width="560" height="315" src="{{youTube5URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
        <h4>{{youTube5}}</h4> 
	
        <iframe width="560" height="315" src="{{youTube6URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
        <h4>{{youTube6}}</h4> 
		
		<iframe width="560" height="315" src="{{youTube7URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
        <h4>{{youTube7}}</h4> 
	
        <iframe width="560" height="315" src="{{youTube8URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
        <h4>{{youTube8}}</h4> 
	
        <iframe width="560" height="315" src="{{youTube9URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>{{youTube9}}</h4> 
	
        <iframe width="560" height="315" src="{{youTube10URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>{{youTube10}}</h4>  
		
		<iframe width="560" height="315" src="{{youTube11URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>{{youTube11}}</h4>  
	
		<iframe width="560" height="315" src="{{youTube12URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>{{youTube12}}</h4> 
	
		<iframe width="560" height="315" src="{{youTube13URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>{{youTube13}}</h4>
	
        <iframe width="560" height="315" src="{{youTube14URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>{{youTube14}}</h4>  
	

        <iframe width="560" height="315" src="{{youTube15URL}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h4>{{youTube15}}</h4> 
   		
	
   	
    	<a name="{{videomenuitem2target}}"></a>
    	<h3>{{video2Title}}</h3>
    	
        <h4>{{youTube16}}</h4> 
	</div>
   
    <div class="clearfix"></div>
</div>

</section>

    {{templateFooter}}
</body>
</html>
