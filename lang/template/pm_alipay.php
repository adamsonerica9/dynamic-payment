<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{templateMeta}}
</head>

<body>
    {{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t pm_bg">{{paymentMethodHeader}}</div>
</div>
</section>

<section id="fh5co-pm">
<div class="container">
    {{templatePaymentMenu}}
    <div class="right_content page_t">
    	<div class="pay_logo"><img src="../images/Alipay_logo.png" alt="{{paymentMethodTitle}}"/></div>
        <h3>{{paymentMethodTitle}}</h3>
        <ul class="f_lis">
        	<li>{{paymentMethodDesc1}}</li>
            <li>{{paymentMethodDesc1}} </li>
        </ul>
    </div>
    <div class="clearfix"></div>
</div>

</section>

    {{templateFooter}}
</body>
</html>
