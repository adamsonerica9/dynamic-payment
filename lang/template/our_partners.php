<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{templateMeta}}
</head>

<body>
    {{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t clients_bg">{{ourPartners}}</div>
</div>
</section>

<section id="fh5co-about">
<div class="container">
    <div class="page_t">
        
        <p>{{ourPartnersContent}}</p>

    </div>
    <h3 class="title_3">{{clients}}</h3>
    <div class="logo_area">    	
   		<div class="logo_f"><img src="../images/partners/logo_Hilton.jpg" alt="Hilton"/></div>
        <div class="logo_f"><img src="../images/partners/logo_shangri.png" alt="Shangri"/></div>
        <div class="logo_f"><img src="../images/partners/logo_meriton.png" alt="Meriton"/></div>
        <div class="logo_f"><img src="../images/partners/logo_Richemont.jpg" alt="Richemont"/></div>
        <div class="logo_f"><img src="../images/partners/logo_Prada.jpg" alt="Prada"/></div>
        <div class="logo_f"><img src="../images/partners/logo_DFS.jpg" alt="DFS"/></div>
        <div class="logo_f"><img src="../images/partners/logo_JR Duty Free.jpg" alt="JR Duty Free"/></div>
        <div class="logo_f"><img src="../images/partners/logo_Escada.jpg" alt="Escada"/></div>
        <div class="logo_f"><img src="../images/partners/logo_Tiffany &amp; Co..jpg" alt="Tiffany"/></div>
        <div class="logo_f"><img src="../images/partners/logo_Ermenegildo Zegna.jpg" alt="Ermenegildo Zegna"/></div>
        <div class="logo_f"><img src="../images/partners/itriplogo.jpg" alt="iTrip.com"/></div>
    </div>
    
    <p>&nbsp;</p>
    
    <h3 class="title_3">{{partners}}</h3>      
    <div class="logo_area">    	      
        <div class="logo_f"><img src="../images/partners/AlipayLogo.jpg" alt="AliPay"/></div>
		<div class="logo_f"><img src="../images/partners/Centermlogo.jpg" alt="Centerm"/></div>
        <div class="logo_f"><img src="../images/partners/Landi.jpg" alt="Landi"/></div>
        <div class="logo_f"><img src="../images/partners/mwLogoTrim.jpg" alt="Merchant Warrior"/></div>
        <div class="logo_f"><img src="../images/partners/Paxlogo.jpg" alt="PAX"/></div>
        <div class="logo_f"><img src="../images/partners/UnionPay_LOGO.jpg" alt="UnionPay"/></div>
        <div class="logo_f"><img src="../images/partners/UnionpayIntllogo.jpg" alt="UnionPay International"/></div>
        <div class="logo_f"><img src="../images/partners/UnionpayQuickPass.jpg" alt="UnionPay Quick Pass"/></div>
        <div class="logo_f"><img src="../images/partners/WeChatLogo.jpg" alt="WeChat Pay"/></div>
    </div>
    <div class="clearfix"></div>
</div>

</section>

    {{templateFooter}}

</body>
</html>
