<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{templateMeta}}    
</head>

<body>
    {{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t aboutus_bg">{{aboutUs}}</div>
</div>
</section>

<section id="fh5co-about">
<div class="container">
    <div class="page_t">
        <h3>{{dynamicPaymentBackgroundHistory}}</h3>
        <p>{{dynamicPaymentBackgroundHistoryDescription}}</p>

		<div class="divider"></div>
		
		<h3>{{serviceCountriesAndAreas}}</h3>
                <p>{{serviceCountriesAndAreasDescription}}</p>

        

        <div class="flag_area">   

        <div class="col-md-4 col-sm-4">     

           <div class="flag_c"><img src="../images/countries/Australia.jpg" alt="Australia National Flag"><span>{{australia}}</span></div>

           <div class="flag_c"><img src="../images/countries/Cambodia.jpg" alt="Cambodia National Flag"><span>{{cambodia}}</span></div>

           <div class="flag_c"><img src="../images/countries/Fiji.jpg" alt="Fiji National Flag"><span>{{fiji}}</span></div>

           <div class="flag_c"><img src="../images/countries/HK.jpg" alt="HK Flag"><span>{{hk}}</span></div>

         </div>

         <div class="col-md-4 col-sm-4">  

           <div class="flag_c"><img src="../images/countries/New_Zealand.jpg" alt="New Zealand National Flag"><span>{{newZealand}}</span></div>

           <div class="flag_c"><img src="../images/countries/Thailand.jpg" alt="Thailand National Flag"><span>{{thailand}}</span></div>

           <div class="flag_c"><img src="../images/countries/Philippines.jpg" alt="Philippines National Flag"><span>{{philippines}}</span></div>

           <div class="flag_c"><img src="../images/countries/Solomon_Islands.jpg" alt="Solomon Islands National Flag"><span>{{solomonIslands}}</span></div>

         </div>

         <div class="col-md-4 col-sm-4"> 

           <div class="flag_c"><img src="../images/countries/Tonga.jpg" alt="Tonga National Flag"><span>{{tonga}}</span></div>

           <div class="flag_c"><img src="../images/countries/Vanuatu.jpg" alt="Vanuatu National Flag"><span>{{vanuatu}}</span></div>

           <div class="flag_c"><img src="../images/countries/Papua.jpg" alt="Papua National Flag"><span>{{papuaNewGinea}}</span></div>
		 </div>
		</div>
       
        <div class="clearfix"></div>
        <div class="divider"></div>
        
        <h3>{{visionStatement}}</h3>
        <p>{{visionStatementDescription}}</p>
        <div class="divider"></div>
        
        <h3>{{whatDynamicDo}}</h3>
        <p>{{whatDynamicDoTitle}}</p>
        <ul class="s_lis">
        	<li>{{list1}}</li>
            <li>{{list2}}</li>
            <li>{{list3}}</li>
            <li>{{list4}}</li>
            <li>{{list5}}</li>
        </ul>
        <div class="divider"></div>
        
    </div>
    <div class="clearfix"></div>
</div>

</section>

    {{templateFooter}}
</body>
</html>
