<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{{templateMeta}}

<body>
	{{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t pm_bg">{{whatsNewHeader}}</div>
</div>
</section>

<section id="fh5co-pm">
<div class="container">
	<div class="left_menu">
    	<ul>
        	<li></li>
        </ul>
    </div>
    <div class="right_content page_t">
    	{{whatsNewDesc}}
    </div>
    <div class="clearfix"></div>
</div>

</section>

	{{templateFooter}}
</body>
</html>
