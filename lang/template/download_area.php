<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {{templateMeta}}
</head>

<body>
    {{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t aboutus_bg">{{downloadArea}}</div>
</div>
</section>

<section id="fh5co-about">
<div class="container">
    <div class="page_t">
        <h3>{{merchantRefund}}</h3>
        <p>
            <i class="downloadicon"><img src="../images/download.svg" alt="Download Icon" /></i><a href="{{posDownloadUrl}}" target="_blank">{{posDownload}}</a><br><br>
		    <i class="downloadicon"><img src="../images/download.svg" alt="Download Icon" /></i><a href="{{upopDownloadUrl}}" target="_blank">{{upopDownload}}</a>
        </p>
        <div class="divider"></div>
		<h3>{{unionPay}}</h3>
		<p>
			<i class="downloadicon"><img src="../images/download.svg" alt="Download Icon" /></i><a href="{{unionPayDownloadUrl}}" target="_blank">{{unionPayDownload}}</a>
		</p>
    </div>
    <div class="clearfix"></div>
</div>

</section>

    {{templateFooter}}

</body>
</html>
