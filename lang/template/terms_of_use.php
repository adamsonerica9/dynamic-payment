<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{{templateMeta}}
</head>

<body>
	{{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t aboutus_bg">{{termsOfUseHeader}}</div>
</div>
</section>

<section id="fh5co-about">
<div class="container">
    <div class="page_t">
        <h3>{{termsOfUseTitle}}</h3>
        <p>
		{{termsOfUseDesc1}}<br /><br />

		{{termsOfUseDesc2}}<br /><br />

		{{termsOfUseDesc3}}<br /><br />

		{{termsOfUseDesc4}} <br /><br />

		{{termsOfUseDesc5}}<br /><br />

		{{termsOfUseDesc6}}<br /><br />

		{{termsOfUseDesc7}}<br /><br />

		{{termsOfUseDesc8}}<br /><br />

		{{termsOfUseDesc9}}<br />
		{{termsOfUseDesc10}} <a href="mailto:{{termsOfUseDesc11}}">{{termsOfUseDesc11}}</a>
		</p>
    </div>
    <div class="clearfix"></div>
</div>

</section>

	{{templateFooter}}
</body>
</html>
