<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{{templateMeta}}
</head>

<body>
	{{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t pm_bg">{{paymentMethodHeader}}</div>
</div>
</section>

<section id="fh5co-pm">
<div class="container">
	{{templatePaymentMenu}}
    <div class="right_content page_t">
		<h3><a id="qrcode_btn">{{paymentMethodTitle1}}</a></h3>
       	<div id="qrcode_layer" class="showlayer">
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_0_1.png" alt="{{paymentMethod1Step1}}" /><br /><br />
				<span class="cirle_step">1</span>
				<strong>{{paymentMethod1Step1}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_0_2.png" alt="{{paymentMethod1Step2}}" /><br /><br />
				<span class="cirle_step">2</span>
				<strong>{{paymentMethod1Step2}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_0_3.png" alt="{{paymentMethod1Step3}}" /><br /><br />
				<span class="cirle_step">3</span>
				<strong>{{paymentMethod1Step3}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_0_4.png" alt="{{paymentMethod1Step4}}" /><br /><br />
				<span class="cirle_step">4</span>
				<strong>{{paymentMethod1Step4}}</strong>
			</div>
			<div class="divider"></div>
		</div>
        
        <h3><a id="online_shopping_btn">{{paymentMethodTitle2}}</a></h3>
        <div id="online_shopping_layer" class="showlayer">
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_1_1.png" alt="{{paymentMethod2Step1}}" /><br /><br />
				<span class="cirle_step">1</span>
				<strong>{{paymentMethod2Step1}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_1_2.png" alt="{{paymentMethod2Step2}}" /><br /><br />
				<span class="cirle_step">2</span>
				<strong>{{paymentMethod2Step2}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_1_3.png" alt="{{paymentMethod2Step3}}" /><br /><br />
				<span class="cirle_step">3</span>
				<strong>{{paymentMethod2Step3}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_1_4.png" alt="{{paymentMethod2Step4}}" /><br /><br />
				<span class="cirle_step">4</span>
				<strong>{{paymentMethod2Step4}}</strong>
			</div>
			<div class="divider"></div>
		</div>
        
        <h3><a id="pos_btn">{{paymentMethodTitle3}}</a></h3>
        <div id="pos_layer" class="showlayer">
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_2_1.png" alt="{{paymentMethod3Step1}}" /><br /><br />
				<span class="cirle_step">1</span>
				<strong>{{paymentMethod3Step1}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_2_2.png" alt="{{paymentMethod3Step2}}" /><br /><br />
				<span class="cirle_step">2</span>
				<strong>{{paymentMethod3Step2}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_2_3.png" alt="{{paymentMethod3Step3}}" /><br /><br />
				<span class="cirle_step">3</span>
				<strong>{{paymentMethod3Step3}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_2_4.png" alt="{{paymentMethod3Step4}}" /><br /><br />
				<span class="cirle_step">4</span>
				<strong>{{paymentMethod3Step4}}</strong>
			</div>
			<div class="divider"></div>
		</div>
        
        <h3><a id="cashier_btn">{{paymentMethodTitle4}}</a></h3>
        <div id="cashier_layer" class="showlayer">
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_3_1.png" alt="{{paymentMethod4Step1}}" /><br /><br />
				<span class="cirle_step">1</span>
				<strong>{{paymentMethod4Step1}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_3_2.png" alt="{{paymentMethod4Step2}}" /><br /><br />
				<span class="cirle_step">2</span>
				<strong>{{paymentMethod4Step2}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_3_3.png" alt="{{paymentMethod4Step3}}" /><br /><br />
				<span class="cirle_step">3</span>
				<strong>{{paymentMethod4Step3}}</strong>
			</div>
			<div class="divi_arrow"><img src="../images/arrow_down.svg" alt="Arrow Down icon" ></div>
			<div class="pm_f_img">
				<img src="../images/pm_flow/pf_3_4.png" alt="{{paymentMethod4Step4}}" /><br /><br />
				<span class="cirle_step">4</span>
				<strong>{{paymentMethod4Step4}}</strong>
			</div>
			<div class="divider"></div>
		</div>
        
    </div>
    <div class="clearfix"></div>
</div>

</section>

	{{templateFooter}}
</body>
</html>
