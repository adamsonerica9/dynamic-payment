<div id="fh5co-footer" role="contentinfo">
	<div class="container ff-width">
    	<div class="row">
         	<div class="col-md-4">
            	<div class="gtco-widget">
					<h3>{{links}}</h3>
					<ul class="gtco-quick-contact">
						<li><a href="index.php">{{home}}</a></li>
						<li><a href="download_area.php">{{downloadArea}}</a></li>
						<li><a href="faq.php">{{faq}}</a></li>
                        <li><a href="whats_new.php">{{whatsNew}}</a></li>
                        <li><a href="videos.php">{{videos}}</a></li>                  
					</ul>
				</div>
            </div>
            <div class="col-md-4">
            	<div class="gtco-widget">
					<h3>{{company}}</h3>
					<ul class="gtco-quick-contact">
						<li><a href="about_us.php">{{aboutUs}}</a></li>
						<li><a href="contact_us.php">{{contactUs}}</a></li>
						<li><a href="terms_of_use.php">{{termOfUse}}</a></li>
                        <li><a href="privacy_statement.php">{{privacyPolicy}}</a></li>
                        <li><a href="dispute_resolution.php">{{disputeResolution}}</a></li>
					</ul>
				</div>
            </div>
            <div class="col-md-4">
            	<div class="gtco-widget">
					<h3>{{followUs}}</h3>
					<div class="social-box">
                    	<a href="#"><img src="../images/wechat.svg" alt="wechat icon" /></a>
                        <a href="https://linkedin.com/company/dynamic-payment" target="_blank"><img src="../images/linkedin.svg" alt="Linkedin icon" /></a>
                    </div>
				</div>
                <img class="foot_logo" src="../images/logo_bw.svg" alt="background Logo" />
                <p>{{copyRight}}</p>
            </div>
        </div>
    </div>
</div>

	<!-- jQuery -->
	<script src="../js/jquery.min.js"></script>
	
	<!-- Bootstrap -->
	<script src="../js/bootstrap.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="../js/jquery.stellar.min.js"></script>
	<!-- Owl Carousel -->
	<script src="../js/owl.carousel.min.js"></script>

	<!-- Main JS (Do not remove) -->
	<!-- <script src="../js/main.js"></script> -->
    <!-- <script src="../js/dropdown.js"></script> -->

	<script type="text/javascript">
		$(document).ready(function(){
			$('#qrcode_layer').hide()
			$('#online_shopping_layer').hide();
			$('#pos_layer').hide();
			$('#cashier_layer').hide();
			
			$('#qrcode_btn').click(function(){
				$('#qrcode_layer').toggle()
				$('#online_shopping_layer').hide();
				$('#pos_layer').hide();
				$('#cashier_layer').hide();
			});

			$('#online_shopping_btn').click(function(){
				$('#qrcode_layer').hide()
				$('#online_shopping_layer').toggle();
				$('#pos_layer').hide();
				$('#cashier_layer').hide();
			});

			$('#pos_btn').click(function(){
				$('#qrcode_layer').hide()
				$('#online_shopping_layer').hide();
				$('#pos_layer').toggle();
				$('#cashier_layer').hide();
			});

			$('#cashier_btn').click(function(){
				$('#qrcode_layer').hide()
				$('#online_shopping_layer').hide();
				$('#pos_layer').hide();
				$('#cashier_layer').toggle();
			});
		});
	</script>	