<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{{templateMeta}}
</head>

<body>
	{{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t solution_bg">{{customizeSolution}}</div>
</div>
</section>

<section id="fh5co-contact">
<div class="container">
    <div class="page_t">
		<h3>{{prepaidCard}}</h3> 
		<p>{{prepaidCardContent}}</p>
		<div class="divider"></div>
		
		<h3>{{integratedSolutions}}</h3>
		<p>{{integratedSolutionsContent}}
		<ul class="s_lis">
			<li>{{integratedSolutionsList1}}</li>
			<li>{{integratedSolutionsList2}}</li>
			<li>{{integratedSolutionsList3}}</li>
			<li>{{integratedSolutionsList4}}</li>
			<li>{{integratedSolutionsList5}}</li>
			<li>{{integratedSolutionsList6}}</li>
		</ul>
		</p>
		<div class="divider"></div>
		
		<h3>{{weChatOfficialAccounts}}</h3>   
        <p>{{weChatOfficialAccountsContent}}
		<ul class="s_lis">
			<li>{{weChatOfficialAccountsList1}}</li>
			<li>{{weChatOfficialAccountsList2}}</li>
			<li>{{weChatOfficialAccountsList3}}</li>
			<li>{{weChatOfficialAccountsList4}}</li>
			<li>{{weChatOfficialAccountsList5}}</li>
			<li>{{weChatOfficialAccountsList6}}</li>
			<li>{{weChatOfficialAccountsList7}}</li>
		</ul>
        <br />
		{{weChatOfficialAccountsDirect}}
		</p> 
		<div class="divider"></div>
		
		<h3>{{billPayment}}</h3>
        <p>{{billPaymentContent}}</p>
		<strong>{{metchant}}</strong>
		<ul class="s_lis">
			<li>{{metchantList1}}</li>
			<li>{{metchantList2}}</li>
			<li>{{metchantList3}}</li>
			<li>{{metchantList4}}</li>
			<li>{{metchantList5}}</li>
		</ul>
        <br />
		<strong>{{member}}</strong>
		<ul class="s_lis">
			<li>{{memberList1}}</li>
			<li>{{memberList2}}</li>
		</ul>		
		<div class="divider"></div>
		
    </div>
    <div class="clearfix"></div>
</div>

</section>


	{{templateFooter}}

</body>
</html>
