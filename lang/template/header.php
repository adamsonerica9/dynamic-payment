<header id="fh5co-header" style="position:fixed;top:0px;left:0px">
	<div class="fluid-container">
    	<nav class="navbar gtco-nav navbar-default">
        	<div class="navbar-header">
            	<!-- Mobile Toggle Menu Button -->
                <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar"><i></i></a>
                <a class="navbar-brand" href="index.php"><img alt="{{dynamicLogo}}" name="Dynamic Payment" src="../images/logo.svg"/></a> 
                <div class="top_payicon">
                    <img alt="{{unionPayLogo1}}" name="UnionPay1" src="../images/top/UnionPay_logo1.png">
                    <img alt="{{wechatPayLogo}}" name="WeChat" src="../images/top/WeChat_s.png">
                    <img alt="{{alipayLogo}}" name="Alipay" src="../images/top/Alipay_s.png">
                    <img alt="{{unionPayLogo2}}" name="UnionPay2" src="../images/top/UnionPay_s.png">
                </div>
 
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="about_us.php"  class="external {{aboutUsActive}}"><span>{{aboutUs}}</span></a></li>
                    <li><a href="payment_method.php" class="external {{paymentMethodActive}}"><span>{{paymentMethod}}</span></a></li>
                    <li><a href="customize_solution.php" class="external {{solutionActive}}"><span>{{solution}}</span></a></li>
                    <li><a href="our_partners.php" class="external {{clientsAndPartnerActive}}"><span>{{clientsAndPartner}}</span></a></li>
                    <li><a href="contact_us.php" class="external {{contactUsActive}}"><span>{{contactUs}}</span></a></li>
                    <li><a href="https://dopp.dynamicg.com/ePay/merchant.action" class="external" target="_blank"><span>{{metchantLogon}}</span></a></li>
                    <li>
                    	<div class="dropdown">
                          <button class="dropbtn"><img src="../images/lang_icon.svg" alt="Language icon" />{{displayLang}}</button>
                          <div class="dropdown-content">
                            <a href="?lang=en" class="external">En</a>
                            <a href="?lang=tc" class="external">繁</a>
                            <a href="?lang=sc" class="external">简</a>
                            <a href="?lang=th" class="external">ไทย</a>
                            <a href="?lang=ca" class="external">ខ្មែរ</a> 
                          </div>
                        </div>
                    </li>
<!--
                    <li>
                    	<div class="dropdown">
                          <button class="dropbtn">Merchant Corner</button>
                          <div class="dropdown-content">
                            <a href="https://dopp.dynamicg.com/ePay/merchant.action" class="external" target="_blank">Merchant Logon</a>						  
                            <a href="https://dopp.dynamicg.com/ePay/merchant.action" class="external" target="_blank">ePay</a>
                            <a href="https://merchant.dynamicg.com/merchantsrv/" class="external" target="_blank">Merchant</a>   
                          </div>
                        </div>
                    </li>
-->					
                </ul>
            </div>
        </nav>
    </div>
</header>