<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{{templateMeta}}
</head>

<body>
	{{templateHeader}}

<section id="fh5co-home" class="top_banner">
<div class="container">
<div class="top_b_t pm_bg">{{paymentMethodHeader}}</div>
</div>
</section>

<section id="fh5co-pm">
<div class="container">
	{{templatePaymentMenu}}
    <div class="right_content page_t">
    	<div class="pay_logo"><img src="../images/UnionPay_logo.png" alt="{{paymentMethodTitle}}"/></div>
       
       
        <h3>{{paymentMethodTitle1}}</h3>
		<p>{{paymentMethod1Desc1}}</p>
		<strong>{{paymentMethod1Desc2}}</strong> 
		<p>{{paymentMethod1Desc3}}</p>
		<strong>{{paymentMethod1Desc4}}</strong>
        <p>{{paymentMethod1Desc5}}</p>
        <div class="divider"></div>

        <h3>{{paymentMethodTitle2}}</h3>
        <p>{{paymentMethod2Desc1}}</p>
        <p>{{paymentMethod2Desc2}}</p>
		<ul class="f_lis">
			<li>{{paymentMethod2Desc3}}
				<ul class="s_lis">
					<li>{{paymentMethod2Desc4}}</li>
					<li>{{paymentMethod2Desc5}}</li>
				</ul>
			</li>
		</ul>                  
             
        <p>{{paymentMethod2Desc6}}</p>	
		<p>{{paymentMethod2Desc7}}</p>  
     	<p></p>         
      	<p>{{paymentMethod2Desc8}}</p>
      	
      	<p>{{paymentMethod2Desc9}}</p>
      	<p><a href="{{paymentMethod2Desc10URL}}">{{paymentMethod2Desc10}}</a></p>
      	
      	<div class="divider"></div>
      	
        <h3>{{paymentMethodTitle3}}</h3>
        <p>{{paymentMethod3Desc1}}</p>
        <ul class="f_lis">
        	<li>
            	{{paymentMethod3Desc2}}
                <ul class="s_lis">
                	<li>{{paymentMethod3Desc3}}</li>
                    <li>{{paymentMethod3Desc4}}</li>
                </ul>
            </li>
            <li>
            	{{paymentMethod3Desc5}}
                <ul class="s_lis">
                	<li>{{paymentMethod3Desc6}}</li>
                    <li>{{paymentMethod3Desc7}}</li>
                </ul>
            </li>          
        </ul>
        <div class="divider"></div>
        <h3>{{paymentMethodTitle4}}</h3>
        <p>{{paymentMethod4Desc1}}</p>
		<p>{{paymentMethod4Desc2}}</p>
		<p><a href="{{paymentMethod4Desc3URL}}">{{paymentMethod4Desc3}}</a>
        </p>
        <div class="divider"></div>
        
        
    </div>
    <div class="clearfix"></div>
</div>

</section>

	{{templateFooter}}
</body>
</html>
