<?php 
	
	// Set default timezone to hong kong
	date_default_timezone_set("Asia/Hong_Kong");

	class util {
		private $debug = true;

		public function setDebug( $flag ) { 
			$this->debug = $flag;
		}

		/**
		 * 	@param array $array Array for beautiful print format
		 */
		public function printr( $array ) {
			if ($this->debug) {
				echo "<pre>";
				print_r($array);
				echo "</pre>";
			}
		}

		public function printJsonString( $array ) {
			$this->printr( json_decode($array) );
		}

		/**
		 * 	Program end if it is cross-origin call
		 */
		public function isDirectLink(){
			if ( !isset($_SERVER["HTTP_REFERER"]) ) die("Invalid call");
		}

		function getYoutubeViedeId($youtubeUrl) {
			/**
			* Pattern matches
			* http://youtu.be/ID
			* http://www.youtube.com/embed/ID
			* http://www.youtube.com/watch?v=ID
			* http://www.youtube.com/?v=ID
			* http://www.youtube.com/v/ID
			* http://www.youtube.com/e/ID
			* http://www.youtube.com/user/username#p/u/11/ID
			* http://www.youtube.com/leogopal#p/c/playlistID/0/ID
			* http://www.youtube.com/watch?feature=player_embedded&v=ID
			* http://www.youtube.com/?feature=player_embedded&v=ID
			*/
			$pattern = 
			  '%                 
			  (?:youtube                    # Match any youtube url www or no www , https or no https
			  (?:-nocookie)?\.com/          # allows for the nocookie version too.
			  (?:[^/]+/.+/                  # Once we have that, find the slashes
			  |(?:v|e(?:mbed)?)/|.*[?&]v=)  # Check if its a video or if embed 
			  |youtu\.be/)                  # Allow short URLs
			  ([^"&?/ ]{11})                # Once its found check that its 11 chars.
			  %i';
			// Checks if it matches a pattern and returns the value
			// if (preg_match($pattern, $youtubeUrl, $match)) {
			// 	echo $match[1];
			//   return $match[1];
			// }
			if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtubeUrl, $match)) {
				return $match[1];
			}
			
			// if no match return false.
			return false;
		}
		
		public function template($content, $replace) {

			if ($replace == null) return $content;

			foreach( $replace as $key => $value ) {
				$content = str_replace( "{{".$key."}}", $value, $content );
			}
			return $content;

		}

		public function langTemplate($content, $replace, $lang) {
			
			if ($replace == null) return $content;
			
			foreach( $replace as $key => $value ) {
				$content = str_replace( "{{".$key."}}", $value[$lang], $content );
			}
			return $content;

		}

		function readTemplate($file){
			ob_start();
			require($file);
			return ob_get_clean();
		}

		function readLang($file){
			ob_start();
			require($file);
			return json_decode(ob_get_clean(),true);
		}

	}

?>