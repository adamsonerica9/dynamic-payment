<?php
    // function to get the current page name
    function PageName() {
        return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
    }
  
    $current_page = PageName();
?>    
    
    <div class="left_menu">
      <ul>
        <li><a href="prepaid.php" class="l_link <?php echo $current_page == 'prepaid.php' ? 'active':NULL ?>">Dynamic Payment Prepaid Card</a></li>
        <li><a href="index.php" class="l_link <?php echo $current_page == 'index.php' ? 'active':NULL ?>">Activation & Enquiry</a></li>
        <li><a href="workflow.php" class="l_link <?php echo $current_page == 'workflow.php' ? 'active':NULL ?>">Prepaid Card Workflow</a></li>
        <li><a href="orderEnquiry.php" class="l_link <?php echo $current_page == 'orderEnquiry.php' ? 'active':NULL ?>">Online Order Enquiry Form (Physical Cards)</a></li>
        <li><a href="terms.php" class="l_link <?php echo $current_page == 'terms.php' ? 'active':NULL ?>">Terms & Conditions</a></li>
        <li><a href="faq.php" class="l_link <?php echo $current_page == 'faq.php' ? 'active':NULL ?>">FAQ & HELP</a></li>
      </ul>
    </div>