<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">FAQ & HELP</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
    <div class="right_content page_t">
      <h3>FAQ & HELP</h3>
      <p><b>Q: Where can buy the physical card and virtual card?</b><br>
	     A: Physical Cards are sold at our authorized outlets, and can be ordered online <a href="orderEnquiry.php" target="_blank">here</a>.</p>
	  <br>
      <p><b>Q: How to activate the card?</b><br>
	     A: You need to activate the Card you purchased before it can be used. You will need to go online at <a href="https://scis.unionpayintl.com/h5scis/41890554/#/" target="_blank">https://scis.unionpayintl.com/h5scis/41890554/#/</a> and follow the instructions to activate the Card.</p>
	  <br>
      <p><b>Q: Where can I use the prepaid card?</b><br>
	     A: The Card can be used for purchases of goods and services, where UnionPay cards are accepted, including at online merchants who accept UnionPay Online Payment (UPOP). However, some merchants may impose their own conditions on the use of the Card, such as minimum payment amounts and surcharges.</p>
	  <br>
      <p><b>Q: How do I check my Card balance?</b><br>
	     A: Card available balance is available through any domestic or global ATM that accepts UnionPay cards. Just insert your card, enter your 6-digit PIN, and select Balance Enquiry from the ATM menu.</p>
	  <br>
      <p><b>Q: Is there a purchase fee on top of the face value of each Card?</b><br>
	     A: Yes, each card denomination has a Suggested Retail Price (SRP) fee, set by the retailers to cover distribution costs. The fees are:
      $50 card = $2.95 , $100 card = $3.95 , $200 card = $4.95 , $500 card = $5.95
        </p>
	  <br>
      <p><b>Q: What is the service hotline?</b><br>
	     A: Please call +679 999 4428</p>
    <br>
      <p><b>Q: Can I buy multiple Cards?</b><br>
	     A: Yes, but in compliance with our license conditions, you can only purchase a maximum of 10 cards per person or a maximum of $5000 in Cards face value, whichever is the greatest.</p>
    </div>
    <div class="clearfix"></div>
  </div>
</section>
<?php
  include("footer.php");
?>

<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>
</body>
</html>
