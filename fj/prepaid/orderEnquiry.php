<!doctype html>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Google reCaptcha -->
<script src="https://www.google.com/recaptcha/api.js"></script>

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->


<style>
	input[type=submit], .captcha{
		width: 100%;
		padding: 12px 15px;
		margin: 8px 0;
		border: none;
		display: inline-block;
		background: #0071E3;
		border-radius: 25px;
		box-sizing: border-box;
		-webkit-transition: 0.5s;
		transition: 0.5s;
		outline: none;
		color: #fff;
	}
	input[type=text]:focus{
		border: 1px solid #65bbff;
	}
	input[type=text]:focus ~ .floating-label,
	input[type=text]:not(:focus):valid ~ .floating-label{
		top: 8px;
		bottom: 10px;
		left: 30px;
		font-size: 10px;
		opacity: 1;
		color: #2b92ef;
	}
	.inputText {
		width: 100%;
		padding: 12px 15px;
 	 	margin: 8px 0;
		display: inline-block;
		border: 1px solid #ccc;
		border-radius: 4px;
		box-sizing: border-box;
		-webkit-transition: 0.5s;
		transition: 0.5s;
		outline: none;
	}
	.floating-label {
		position: absolute;
		pointer-events: none;
		left: 30px;
		top: 20px;
		transition: 0.2s ease all;
	}
	.qtydiv{
		background: #fff;
		padding: 12px 15px;
		margin-left: 15px;
		margin-top:20px;
		border-radius: 50px;
		/* width: 31%; */
	}
	.button {
		background-color: white;
		border: solid 1px #ccc;		
		text-align: center;
		display: inline-block;
		cursor: pointer;
		border-radius: 50%;
		width: 2.25em;
		height: 2.25em;
	}
	.qty{
		border: none;
		width: 2.25em;
		height: 2.25em;
		text-align: center;
	}

	.minus img{
		opacity: 0.5;
	}

	.plus img{
		opacity: 0.5;
	}

	.productPadding {
		float:left; 
		padding-left:25px;
	}

	.border-container {
		padding: 12px 15px;
	}

	.border {
		background: #fff;
		padding: 12px 15px;
		/* margin-left: 15px; */
		margin-top:20px;
		border-radius: 50px;
		width:98%;
	}

</style>
	<script>
		function onSubmit(token) {
			console.log(token);
			document.getElementById("EnquiryForm").submit();
		}
	</script>
</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">Online Order Enquiry Form (Physical Cards)</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
    <div class="right_content page_t">
      <h3>Online Order Enquiry Form (Physical Cards)</h3>

	  	<div class="row">
		    <div class="col-md-12">
			Please note that the order is not fulfilled until full payment is received via cleared funds to our bank account.
			</div>
		  	<div class="col-md-6">
			  	<br><br>
			  	<table style="width: 100%; background-color:#EEE; border-collapse:collapse;">
					<tr style="border-bottom: 1px solid #ccc; text-align:center;">
						<td style="padding: 6px; width:33%;">Card Value (FJD)</td>
						<td style="padding: 6px; width:33%;">Card Fee (FJD)</td>
						<td style="padding: 6px; width:33%;">Total Price (FJD)</td>
					</tr>
					<tr style="border-bottom: 1px solid #ccc; text-align:center;">
						<td style="padding: 6px; width:33%;">50.00</td>
						<td style="padding: 6px; width:33%;">2.95</td>
						<td style="padding: 6px; width:33%;">52.95</td>
					</tr>
					<tr style="border-bottom: 1px solid #ccc; text-align:center;">
						<td style="padding: 6px; width:33%;">100.00</td>
						<td style="padding: 6px; width:33%;">3.95</td>
						<td style="padding: 6px; width:33%;">103.95</td>
					</tr>
					<tr style="border-bottom: 1px solid #ccc; text-align:center;">
						<td style="padding: 6px; width:33%;">200.00</td>
						<td style="padding: 6px; width:33%;">4.95</td>
						<td style="padding: 6px; width:33%;">204.95</td>
					</tr>
					<tr style="border-bottom: 1px solid #ccc; text-align:center;">
						<td style="padding: 6px; width:33%;">500.00</td>
						<td style="padding: 6px; width:33%;">5.95</td>
						<td style="padding: 6px; width:33%;">505.95</td>
					</tr>
				</table>
				<br>
				<em>*Delivery Fee: 6.50 (FJD) per order, anywhere on Viti Levu. Please contact us for delivery pricing to other areas.</em>
			</div>
			<div class="col-md-6">
				<br>
				<img src="images/prepaidcard_combined.svg">
			</div>	
			<div class="col-md-12">
				<hr>
			</div>

			<FORM action="../../mailClient/mailClient.php" method="POST" name="EnquiryForm" id="EnquiryForm">
				<input type="hidden" name="nation" value="nz">
				<!--mailto:dpnz@dynamicg.com?subject=Online Order Enquiry Form (Physical Prepaid Cards)//-->
				<div class="row">
					<div class="col-md-12">
						Order confirmation will be sent via email; our staff will contact you directly.
					</div>
					<div id="errorMsg" class="col-md-12" style="color:red;display:none">
						Please fill in all the fields with *
					</div>
					<div class="col-md-3">
						<input type="text" name="firstName" id="firstname" class="inputText" required>
						<span class="floating-label"><span style="color:red">* </span>First Name</span>
					</div>
					<div class="col-md-3">
						<input type="text" name="lastName" id="lastname" class="inputText" required>
						<span class="floating-label"><span style="color:red">* </span>Last Name</span>
					</div>
					<div class="col-md-3">
						<input type="text" name="email" id="email" class="inputText" required>
						<span class="floating-label"><span style="color:red">* </span>Email Address</span>
					</div>
					<div class="col-md-3">
						<input type="text" name="phoneNumber" id="phone" class="inputText" required>
						<span class="floating-label"><span style="color:red">* </span>Phone Number</span>
					</div>
					<div class="col-md-12">
						<input type="text" name="companyName" id="company" class="inputText" required>
						<span class="floating-label"><span style="color:red">* </span>Company Name</span>
					</div>
					<div class="col-md-12">
						<input type="text" name="billingAddress" id="billingaddress" class="inputText" required>
						<span class="floating-label"><span style="color:red">* </span>Billing Address</span>
					</div>
					<div class="col-md-12">
						<input type="text" name="shippingAddress" id="shippingaddress" class="inputText" required>
						<span class="floating-label"><span style="color:red">* </span>Shipping Address</span>
					</div>
					<div class="col-xs-12 col-lg-3">
						<div class="row border">
							<div style="float:left; margin-left:5px;">
								<strong>Product (FJD)</strong><br>
								50.00
							</div>
							<div class="productPadding">
								<button class="button minus" name="product1minus" id="product1minus" onclick="return false;"><img src="images/minus.svg"></button>
								<input type="text" value="0" class="qty" name="product1qty" id="product1qty">
								<button class="button plus" name="product1plus" id="product1plus" onclick="return false;"><img src="images/plus.svg"></button>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-3">
						<div class="row border">
							<div style="float:left; margin-left:5px;">
								<strong>Product (FJD)</strong><br>
								100.00
							</div>
							<div class="productPadding">
								<button class="button minus" name="product2minus" id="product2minus" onclick="return false;"><img src="images/minus.svg"></button>
								<input type="text" value="0" class="qty" name="product2qty" id="product2qty">
								<button class="button plus" name="product2plus" id="product2plus" onclick="return false;"><img src="images/plus.svg"></button>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-3">
						<div class="row border">
							<div style="float:left; margin-left:5px;">
								<strong>Product (FJD)</strong><br>
								200.00
							</div>
							<div class="productPadding">
								<button class="button minus" name="product2minus" id="product2minus" onclick="return false;"><img src="images/minus.svg"></button>
								<input type="text" value="0" class="qty" name="product2qty" id="product2qty">
								<button class="button plus" name="product2plus" id="product2plus" onclick="return false;"><img src="images/plus.svg"></button>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-3">
						<div class="row border">
							<div style="float:left; margin-left:5px;">
								<strong>Product (FJD)</strong><br>
								500.00
							</div>
							<div class="productPadding">
								<button class="button minus" name="product2minus" id="product2minus" onclick="return false;"><img src="images/minus.svg"></button>
								<input type="text" value="0" class="qty" name="product2qty" id="product2qty">
								<button class="button plus" name="product2plus" id="product2plus" onclick="return false;"><img src="images/plus.svg"></button>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<hr>
					</div>
				</div>
				<div class="row">
					<br class="mobileBr">
					<button class="g-recaptcha captcha" 
						data-sitekey="6Ldl7nYnAAAAAOyzTlco6lTD-IVX_Sc_d9sjT6DH"
						data-callback='onSubmit' 
						data-action='submit'>Submit</button>
						<!-- //-->
						<!-- <input type="submit" value="Submit" name="formsubmit" id="formsubmit" disabled> -->
				</div>
			</FORM>

		</div>

    </div>
    <div class="clearfix"></div>
  </div>
</section>
<?php
  include("footer.php");
?>

<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>

<script src="https://www.google.com/recaptcha/api.js"></script>

<script>

	function onSubmit(response) {

		var fail = false;
		$(".inputText")
			.each( function(){
				if ( !$(this).val() ) {
					console.log("Test");
					$(this).css("border","1px solid red");
					$("#errorMsg").css("display","block");
					fail = true;
				}
			});
		
		console.log("Form submit response", response);

		if (fail) {
			console.log("fail");
		} else {
			$("form").submit();
		}

	}

	$(".inputText")
		.on("focusout",function() {

			console.log($(this).val());

			if ( !$(this).val() ) {
				console.log("Test");
				$(this).css("border","1px solid red");
				$("#errorMsg").css("display","block")
			}

		})
		.on("focus",function() {

				$(this).css("border","1px solid #ccc");

		})


	$("#product1plus").on( "click", function() {
		var newQty = +($("#product1qty").val()) + 1;
		$("#product1qty").val(newQty);
	});
		
	$("#product1minus").on( "click", function() {
		var newQty = +($("#product1qty").val()) - 1;
		if(newQty < 0)newQty = 0;
		$("#product1qty").val(newQty);
	});	

	$("#product2plus").on( "click", function() {
		var newQty = +($("#product2qty").val()) + 1;
		$("#product2qty").val(newQty);
	});
		
	$("#product2minus").on( "click", function() {
		var newQty = +($("#product2qty").val()) - 1;
		if(newQty < 0)newQty = 0;
		$("#product2qty").val(newQty);
	});	

	$("#product3plus").on( "click", function() {
		var newQty = +($("#product3qty").val()) + 1;
		$("#product3qty").val(newQty);
	});
		
	$("#product3minus").on( "click", function() {
		var newQty = +($("#product3qty").val()) - 1;
		if(newQty < 0)newQty = 0;
		$("#product3qty").val(newQty);
	});	
	
</script>


</body>
</html>
