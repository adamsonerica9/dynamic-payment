<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">Dynamic Payment Prepaid Card</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
    <div class="right_content page_t">
      <h3>Prepaid Card Flow Overview</h3>

      <div class="row" style="width:100%">

        <!-- Card management -->
        <div class="col-md-5" style="padding-left:0px; padding-right:0px;">

          <!-- Title -->
          <div class="row" style="width:100%">
            <div class="col-md-12">
              <div class="row" style="border: 1px solid rgba(222, 222, 222, 1);border-radius: 45px;padding: 10px;">
                <div class="col-md-2" style="padding-left:5px; padding-right:5px;">
                  <img src="images/dpwalletlogo.png" alt="DP Wallet Logo" width="40em">
                </div>
                <div class="col-md-10" style="padding-left:0px; padding-right:0px;">
                  <strong><em>CARD MANAGEMENT</em></strong><br>
                  Cardholders link the Card in DP Wallet app
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <img src="images/cardholder_vertification.svg" alt="Cardholder Vertification" width="150em"><br>
                  Cardholder Verification
                </div>
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <br>
                  <br>
                  <img src="images/arrowdown.svg" alt="Arrow Down" width="20em">
                  <br>
                  <br>
                  <br>
                </div>
                <div class="col-xs-3">
                  <br>
                  <img src="images/dpwalletapp.png" alt="DP Wallet app" width="30em">
                  <br>
                  DP Wallet app
                </div>  
                <div class="col-xs-1">
                  <br>
                  <img src="images/arrowright.svg" alt="Arrow Left" style="transform: rotate(180deg);" height="20em">
                  <br>
                </div>  
                <div class="col-xs-3" style="text-align:center;">
                  <br>
                  Link up Card
                  <br>
                </div>  
                <div class="col-xs-1">
                  <br>
                  <img src="images/arrowright.svg" alt="Arrow Right" height="20em">
                  <br>
                </div>  
                <div class="col-xs-3">
                  <br>
                  <img src="images/prepaid_front.svg" alt="Prepaid Card Front" width="100em">
                  <br>
                  DP Prepaid Card
                </div>  
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <img src="images/arrowdown.svg" alt="Arrow Down" width="20em">
                  <br>
                </div>
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <img src="images/top_up.svg" alt="Top Up" width="150em">
                  <br>
                  Top Up
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="col-md-1" style="height:auto;top:30px;margin:0px;padding:0px">
              <div style="width:25px;height:100%;border-top:1px solid;border-right:1px solid; border-top-right-radius: 15px">&nbsp;</div>
              <div style="width:25px;height:50em;border-right:1px solid;">&nbsp;</div>
        </div>


        <!-- Card payment -->
        <div class="col-md-5" style="padding-left:0px; padding-right:0px;">
            
          <!-- Title -->
          <div class="row" style="width:100%">
            <div class="col-md-12">
              <div class="row" style="border: 1px solid rgba(222, 222, 222, 1);border-radius: 45px;padding: 10px;">
                <div class="col-md-2" style="padding-left:5px; padding-right:5px;">
                  <img src="images/checked.svg" alt="Checked" width="40em">
                </div>
                <div class="col-md-10" style="padding-left:0px; padding-right:0px;">
                  <strong><em>CARD PAYMENT</em></strong><br>
                  Use Physical card or DP Wallet app
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <img src="images/dp_wallet_app_scan_QR.svg" alt="DP Wallet app Scan QR code/NFC (Android phone)" width="150em"><br>
                  DP Wallet app Scan QR code/NFC (Android phone)
                </div>
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <img src="images/arrowup.svg" alt="Arrow Up" width="20em">
                  <br>
                  Or
                  <br>
                  <img src="images/arrowdown.svg" alt="Arrow Down" width="20em">
                  <br>
                </div>
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <img src="images/card_tap_insert_swipe.svg" alt="Card Tap, Insert or Swipe" width="150em"><br>
                  Card Tap, Insert or Swipe
                </div>
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <img src="images/arrowdown.svg" alt="Arrow Down" width="20em">
                  <br>
                </div>
                <div class="col-md-12" style="text-align:center;">
                  <br>
                  <img src="images/transaction_completed.jpg" alt="Transaction Completed" width="150em"><br>
                  Transaction Completed
                </div>
                
              </div>
            </div>
          </div>

        </div>

        <div class="col-md-1" style="height:auto;top:30px;margin:0px;padding:0px">
              <div style="width:25px;height:100%;border-top:1px solid;border-right:1px solid; border-top-right-radius: 15px">&nbsp;</div>
              <div style="width:25px;height:50em;border-right:1px solid;">&nbsp;</div>
        </div>

      </div>


      
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="divider"></div>
  </div>
</section>
<?php
  include("footer.php");
?>

<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>
</body>
</html>
