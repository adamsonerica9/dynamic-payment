<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">Terms & Conditions</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
    <div class="right_content page_t">

      <h3>UnionPay Gift Card Terms and Conditions</h3>
	  
      <p>The UnionPay Gift Card ('Card') is issued by Dynamic Payment Pty Ltd ("DP") pursuant to license by UnionPay International. By purchasing or using the Card you agree to the terms and conditions stated herein ("Terms"). In these Terms, 'you' means the Card purchaser or user.</p>
      <ul class="f_lis">
		<li><b>Stored value:</b> The Card is a prepaid card loaded with a specific amount of funds when you purchased it. It cannot be reloaded with additional value. Physical Cards are sold at our authorized stores and can also be purchased online at: <a target='_blank' href="orderEnquiry.php">https://www.dynamic-payment.com/fj/prepaid/orderEnquiry.html</a></li>
        <li><b>Activation:</b> Follow the instructions. </li>
		<li><b>PIN:</b> After you activate your Card online, you may access your PIN through the scratch panel on the back of the Card.</li>
        <li><b>Card balance:</b> Details of your Card balance is available through any ATM that accepts UnionPay cards. Just insert your card, enter your 6-digit PIN, and select Balance Enquiry from the ATM menu.</li>
		<li><b>Expiry:</b> The Card is valid until the date printed on the front of the Card. Any remaining value on the Card is forfeited and becomes unusable automatically when the Card expires.</li>
        <li><b>Using the Card:</b><br>
           (a) The Card can be used for purchases of goods and services, where UnionPay cards are accepted, including at online merchants who accept UnionPay Online Payment (UPOP). However, some merchants may impose their own conditions on the use of the Card, such as minimum payment amounts and surcharges.<br><br>
           (b) The Card cannot be used to redeem or withdraw cash at any ATMs or over the counter at financial institutions.<br><br>
           (c) Each time you use the Card, you authorise us to deduct the amount of the transaction from the balance of funds on the Card. The available balance cannot be exceeded. Where a purchase exceeds the available balance, the excess must be paid using other payment method. The Card cannot be used for direct debit, recurring or instalment payments.<br><br>
           (d) You cannot stop payment on any transaction made with the Card.<br><br>
		   (f) All transactions in foreign currency will be converted into Fiji dollars. Transactions will either be converted directly into Fiji dollars or will be first converted from the currency in which the transaction was made into US dollars and then converted to Fiji dollars at a rate determined by UnionPay International. A fee of 2% of the total amount of each transaction will also be deducted from your Card for any transaction in foreign currency or any transaction in any currency (including FJD) that is billed by a merchant outside of Fiji.<br><br>
           (g) You are responsible for all transactions on the Card, except where there has been fraud or negligence by our staff or agents. If you notice any error relating to the Card, you should notify us immediately at +679 999 4428 or email us at <a target='_blank' href="mailto:dpfj@dynamicg.com">dpfj@dynamicg.com</a> 
		</li>
        <li><b>Lost or stolen card:</b> You must take reasonable care to safeguard your Card against loss, theft, or misuse. A Card is anonymous and is similar to cash. We are not able to replace the Card or refund the balance in the event the Card is misused, lost, stolen, or damaged.</li>
		<li><b>Complaint:</b> If you have a complaint about the Card, or service, please call Dynamic Payment on +679 999 4428. If you have any enquiries or complaints in relation to your Card, you should, in the first instance, contact Dynamic Payment. If you feel that your complaint has not been addressed to your satisfaction, you may escalate the matter to UnionPay International. The contact details for UnionPay International are +61 2 9250 8888
		</li>
        <li><b>Suspension:</b> For security reasons, including where we have reason to suspect fraud or other illegal activity, we may suspend the Card, prevent a transaction, or stop the use of the Card.</li>
        <li><b>Disclosure:</b> We may disclose information about the Card or transactions made with the Card to third party whenever allowed by law, required by law or where it is necessary to operate the Card and process transactions.</li>  
        <li><b>Amendment:</b> We reserve the right to change these Terms at any time by posting a notice on our website <a target='_blank' href="https://www.dynamic-payment.com">https://www.dynamic-payment.com</a> at least 60 days prior to the effective day of the change.
		</li>
        <li><b>Our rights to assign:</b> We can assign any or all of our rights under these terms and conditions to any other party.
		</li>
        <li><b>Limits of our liability:</b> Our liability to you in connection with the Card, if any, will be limited to the unused balance that remains on your Card. Under no circumstances will we be liable for any indirect or consequential losses, even if advised of the possibility of such losses.
		</li>
        <li><b>Partial invalidity:</b> If any of these terms are found to be unenforceable, it shall not affect the validity of the rest of these terms.
		</li>
        <li><b>Governing law and jurisdiction:</b> The laws of Fiji apply to these Terms and you irrevocably submit to and accept the exclusive jurisdiction of any of the Courts of Fiji.
		</li>
        <li>Gift cards (stored-value cards) are excluded from change-of-mind items, please read the Ts&Cs carefully to ensure you are fully aware of your rights under the policy and our obligations to you.</li>
      </ul>
	  
      <h3>e-Wallet Terms and Use</h3>
	  
      <p>These Terms of Use apply to the e-Wallet Mobile Application owned by Dynamic Payment Pty Ltd ('DP'). Please read these Terms of Use carefully before using the DP e-Wallet Mobile Application ('App').</p>
      <p>By using the App you are deemed to have read and accepted these Terms of Use. Please refrain from using the App if you do not agree to all or any of these Terms of Use. DP shall not be liable for payment of any costs or expenses incurred as a result of downloading and using the App, including any operator network and/or roaming charges.</p>  

      <ul class="f_lis">
		<li>
            <b>Intellectual Property</b><br>
            (a) The contents of the DP App are intended for your personal non-commercial use only. Graphics and images on the App are protected by copyright and may not be reproduced, translated or appropriated in any manner without our written permission. Modification of any of the materials or use of the materials for any other purpose will be a violation of DP's copyright and other intellectual property rights and the copyright and intellectual property rights of the respective owners.<br><br>
            (b) Nothing in the Terms of Use herein gives you any right, title or interest in any intellectual property rights relating to the App, including any brands, trademarks (whether registered or otherwise), music or soundbites and/or images and designs. You may use the material on the App for the purpose of accessing DP e-Wallet services only. You must not duplicate, copy, reproduce, reverse-engineer, distribute, transmit, adapt, republish, display, or use the App or any of its contents, in any form or manner, except to use it for the said purpose.
        </li>
        <li>
            <b>Use of the App</b><br>
            (a) You agree to use the App in accordance with these Terms of Use and in a lawful manner and for its intended purpose. You agree to be responsible for all matters arising from your use of the App. Further, you agree not to use the App in any manner which may breach any applicable law or regulations or causes or which may cause an infringement of any third party rights, not to post, transmit or disseminate any information on or via the App which may be harmful, obscene, defamatory or illegal or create liability on our part, not to interfere or attempt to interfere with the operation or functionality of the App and not to obtain or attempt to obtain unauthorised access, via any means, to any of DP's systems.<br><br>
            (b) If we have any reason to believe that you are in breach, or will be in breach, of any of these Terms of Use, we reserve the right to terminate your access to the App immediately.
        </li>
        <li>
            <b>Other Sites</b><br>
            The App may provide links to other sites and vice versa merely for your convenience and information. We shall neither be responsible for the content and availability of such other applications that may be operated and controlled by third parties and by our various branch offices and affiliates, nor for the information, products or services contained on or accessible through those mobile applications. Your access and use of such mobile applications remain solely at your own risk. DP will not be liable for any direct, indirect, consequential losses and/or damages of any kind arising out of your access to such applications.
        </li>
        <li>
            <b>No Warranties</b><br>
            We provide no warranty, whether expressly or implied, of any kind, including but not limited to any implied warranties or implied terms of satisfactory quality, fitness for a particular purpose or non-infringement. To the extent permitted by law, all such implied terms and warranties are hereby excluded.
        </li>
        <li>
            <b>Disclaimer</b><br>
            We do not warrant that the functions or operation of the App will be uninterrupted or error free, or that defects will be corrected or that the App or the server that makes it available is free of any virus or other harmful elements. While we use reasonable efforts to include accurate and up-to-date information on the App, we do not warrant or make any representations regarding the correctness, accuracy, reliability or otherwise of the materials in the App or the results of their use.
        </li>
        <li>
            <b>Liability</b><br>
            (a) To the extent permitted by law, we do not accept any liability or responsibility whatsoever if the App is not available, complete or error-free over any period or at any particular time.<br><br>
            (b) By using the App you agree that DP will not be liable under any circumstances, including negligence, for any direct, indirect or consequential loss arising from your use of the information and material contained in the App or from your access to any of the linked sites. We are not liable nor responsible for any material provided by third parties with their own respective copyright and shall not under any circumstances, be liable for any loss, damages or injury arising from these materials.<br><br>
            (c) The information contained in the App is for informational purposes only and is provided to you on an "as-is" basis. We do not guarantee the accuracy, timeliness, reliability, authenticity of completeness of any of the information contained on the App. We are not liable for any information or services which may appear on any linked mobile applications or websites.
        </li>
        <li>
            <b>Security</b><br>
            The App may require you to choose and input a personal identification number ('PIN'). You are responsible for keeping that PIN safe and secure so as to prevent any unauthorised use of and access to your e-Wallet. You will be responsible for all use of your e-Wallet through the App and all purchases made through your e-Wallet notwithstanding any unauthorised access.
        </li>
        <li>
            <b>Changes and Modifications</b><br>
            (a) We reserve the right at our absolute discretion and without liability to change, modify, alter, adapt, add or remove any of the terms and conditions contained herein and/or change, suspend or discontinue any aspect of the App. The revised term shall take effect from the time it is posted.<br><br>
            (b) We are not required to give you any advanced notice prior to incorporation of any of the above changes and/or modifications into the App.
        </li>
        <li>
            <b>Privacy</b>
            (a) To use the App, you are required to provide certain personal information. We collect, process and use personal information in accordance with these Terms of Use and our Privacy Policy, which can be found at <a target='_blank' href="https://www.dynamic-payment.com/en/privacy_statement.html">https://www.dynamic-payment.com/en/privacy_statement.html</a><br><br>
            (b) By accessing the App and the use of services offered on the App, you are deemed to have accepted our Privacy Policy and the terms herein.<br><br>
            (c) In addition to the purposes listed in our Privacy Policy, we may use any information you provide us to:
            <ul class="s_lis">
                <li>provide you e-Wallet service through the App (including by disclosing personal information to our authorised third-party contractors);</li>
                <li>contact you with information regarding the App;</li>
                <li>monitor your use of the App;</li>
                <li>help resolve and manage issues with the App;</li>
                <li>report on App usage to help us improve the App;</li>
                <li>conduct market research to better understand your needs in relation to the App and the DP e-Wallet service;</li>
                <li>personalise any of our services and products for you and to deliver targeted advertisements and offers from us and third parties;</li>
                <li>identify offers, promotions, new products, improvements, or other information we think you might find interesting, tell you about them, and provide them to you whether through the App or other channels such as e-mail.</li>
            </ul>    
        </li>
        <li>
            <b>Applicable Law and Jurisdiction</b><br>
            These Terms of Use and this Mobile App's content shall be governed and construed in accordance with Fiji laws, and the courts of Fiji shall have exclusive jurisdiction to adjudicate any dispute which may arise in relation thereto.
        </li>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</section>
<?php
  include("footer.php");
?>
<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>
</body>
</html>
