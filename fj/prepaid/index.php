<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

<style>
.button {
  border: none;
  color: white;
  padding: 12px 40px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 14px;
  border-radius: 50px;
  background-color: #0071E3;
}

.brstyle {
  display: block; 
  content: " ";
  margin-top: 5px;
}

@media screen and (max-width: 1200px) {
  .walletimg{
    width:200em;
  }

  .invisible-desktop{
        display: block;
    }
    .invisible-mobile{
        display: none;
    }
}

@media screen and (max-width: 1024px) {
  .walletimg{
    width:100%;
  }

  
}
</style>

</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">Dynamic Payment Prepaid Card</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
    <div class="right_content page_t">
      <h3>Activation & Enquiry</h3>

      <div class="row">
        <div class="col-md-6">
		      
          <strong><em>Prepaid Card Activation</em></strong><br />

          <p>Please click the "Start" to activate your Prepaid card.</p>
     
          <button onclick='window.open("https://scis.unionpayintl.com/h5scis/42870242/#/","_blank","","")' class="button">Start</button>
        </div>
        <div class="col-md-6">
          <img src="images/prepaidcard_phone.svg" alt="Activation" width="350em">
        </div> 
      </div>
        
      <hr>
      
      <div class="row">
        <div class="col-md-3">
          <img src="images/prepaidcard_app.svg" alt="DP Wallet Logo" width="175em">
        </div>
        <div class="col-md-9">
          <h2>DP Wallet</h2>
          <a href="https://apps.apple.com/au/app/dp-wallet-global/id1546171312" target="_blank"><img src="images/iOS_download_logo_en.png" alt="Apple AppStore" height="45em">&nbsp;</a>
          <a href="https://play.google.com/store/apps/details?id=com.upi.dp_wallet&pcampaignidc=apps&hl=en&gl=US" target="_blank"><img src="images/android_download_logo_en.png" alt="Google Play" height="45em"><br class="brstyle"></a><br>
          <strong>1.</strong> Search <strong>"DP Wallet Global"</strong> in AppStore or Google Play on your mobile device and press "Download" button to get DP Wallet Global
        </div>
      </div>
        
      <hr>
    
      <div class="row">
        <div class="col-md-3">
          <img src="images/wallet_step1.svg" alt="Registration" class="walletimg"><br><br>
          <strong>2. Registration</strong><br class="brstyle">
          Use telephone number or email address to register an account
        </div>
        <div class="col-md-1 invisible-mobile" style="padding-top:25%; padding-left:5.5em; text-align: right;">
          <img src="images/right_logo.svg" alt="Arrow Right" height="20em">
        </div>
        <div class="col-md-12 invisible-desktop fea_txt">
          <img src="images/arrowdown.svg" alt="Arrow Down" width="20em"><br><br><br>
        </div>
        <div class="col-md-3">
          <img src="images/wallet_step2.svg" alt="Virtual Card Purchase" class="walletimg"><br><br>
          <strong>3. Virtual Card Purchase</strong><br class="brstyle">
          Payment available with "PoliPay" and "Alipay"
        </div>
        <div class="col-md-1 invisible-mobile" style="padding-top:25%; padding-left:5.5em; text-align: right;">
          <img src="images/right_logo.svg" alt="Arrow Right" height="20em">
        </div>
        <div class="col-md-12 invisible-desktop fea_txt">
          <img src="images/arrowdown.svg" alt="Arrow Down" width="20em"><br><br><br>
        </div>
        <div class="col-md-3">
          <img src="images/wallet_step3.svg" alt="Applied Successfully" class="walletimg"><br><br>
          <strong>4. Applied Successfully</strong><br class="brstyle">
          Applied successfuly and click the "View cards" button
        </div>
        <div class="col-md-1" width="0px">
        </div>
      </div>
      
      <div class="clearfix"></div>
      <div class="divider"></div>
    </div>
  </div>
</section>
<?php
  include("footer.php");
?>

<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>
</body>
</html>
