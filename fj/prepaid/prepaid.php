<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Dynamic Payment</title>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="../../../images/favicon.ico">
<!-- Animate.css -->
<link rel="stylesheet" href="../../../css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../../css/icomoon.css">
<!-- Simple Line Icons -->
<link rel="stylesheet" href="../../../css/simple-line-icons.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../../css/bootstrap.css">
<!-- Owl Carousel  -->
<link rel="stylesheet" href="../../../css/owl.carousel.min.css">
<link rel="stylesheet" href="../../../css/owl.theme.default.min.css">
<!-- Style -->
<link rel="stylesheet" href="../../../css/style.css">

<!-- Modernizr JS -->
<script src="../../../js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>
<?php
  include("header.php");
?>
<section id="fh5co-home" class="top_banner">
  <div class="container">
    <div class="top_b_t pm_bg">Prepaid Card</div>
  </div>
</section>
<section id="fh5co-pm">
  <div class="container">
<?php
  include("leftmenu.php");
?>
  <div class="right_content page_t">
	
    <h3>Prepaid Card</h3>
	
    <div class="row">
      <div class="col-md-12">
        <img src="images/prepaidcard_cover.svg" alt="Prepaid Card Front Image" width="200em">
        &nbsp;
        <img src="images/prepaidcard_backcover.svg" alt="Prepaid Card Back Image" width="200em">   
        <br><br>
        <i class="downloadicon"><img src="../../../images/download.svg" alt="Download Icon" /></i><a href="pdf/userGuide.pdf" target="_blank">DP UnionPay Prepaid Card User Guide (Chinese Version Only)</a>
      </div>
    </div>
  
    <hr>

    <div class="row">
      <div class="col-md-12">
        <p>A simply plastic alternative to carrying money around, these are often called Gift Cards.
          <ul class="s_lis">
            <li>	Global UnionPay Network Acceptance</li>
            <li>Fixed amounts: $50 $100 $200 $500</li>
            <li>Card Purchase fees: $50 = $2.95, $100 = $3.95, $200 = $4.95, $500 = $5.95</li>
            <li>Balance enquiry at ATM fee: $0.60</li>
            <li>Foreign currency conversion fee: 2.0% of the total transaction amount</li>
          </ul>
        </p>
        *Not redeemable for cash, no cash withdrawals allowed at ATMs, Non-reloadable<br>
      </div>
    </div>

	<br><br>
	<p>
    </p>
<!--	
    <div class="fagcard_con"> <img src="../../../nz/prepaid/images/cardfront.png" alt="Card Front.png"/> <img src="../../../nz/prepaid/images/cardback.png" alt="Card Back.png"/> <img src="../../../nz/prepaid/images/dp_unionlogo.png" alt="DP_Union Logo.png"/> </div>
    <ul class="s_lis">
      <li>Global UnionPay@ Network Acceptance</li>
      <li>Not redeemable for cash Not redeemable for cash Not redeemable for cash</li>
      <li>Not used at ATMs Not used at ATMs</li>
      <li>Non -reloadablereloadable</li>
      <li>Load amount: $50 -$1,000</li>
      <li>One -off purchase fee: $3.95</li>
      <li>FREE balance and transaction enquiry via website & DP wallet app</li>
      <li>Foreign currency conversion fee: 3.5% of the total transaction amount</li>
    </ul>
    <div class="clearfix"></div>
-->	
  </div>
</section>
<?php
  include("footer.php");
?>

<!-- jQuery --> 
<script src="../../../js/jquery.min.js"></script> 
<!-- Bootstrap --> 
<script src="../../../js/bootstrap.min.js"></script> 
<!-- Stellar Parallax --> 
<script src="../../../js/jquery.stellar.min.js"></script> 
<!-- Owl Carousel --> 
<script src="../../../js/owl.carousel.min.js"></script> 

<!-- Main JS (Do not remove) --> 
<script src="../../../js/main.js"></script> 
<script src="../../../js/dropdown.js"></script>
</body>
</html>
