<?php
	session_start();

	$page = "paymentMethod";

	include "phpclass/util.php";
	include "lang.php";
	
	// Read templates
	$templateIndex = $util->readTemplate("lang/template/pm_wechat.php");
	
	// Read language json
	$langIndex = $util->readLang("lang/pm_wechat.json");
	
	/** 
	 * 	Page content
	*/
	$templateIndex = $util->langTemplate($templateIndex, $langIndex, $lang);

	// Payment menu active
	$templateData["payment"] = "";
	$templateData["alipay"] = "";
	$templateData["unionpay"] = "";
	$templateData["wechat"] = "active";

	$display = $util->template($templateIndex, $templateData);
	
	echo $display;

?>