<?php

    $util = new Util();

    // Read current lang, set a default lang if it's empty
	$currentLang = ( isset($_SESSION["dynamicPayment"]["lang"]) ) ? $_SESSION["dynamicPayment"]["lang"] : "en";
	
	// Chage lange if there is request lang
	$lang = ( isset($_REQUEST["lang"]) ) ? $_REQUEST["lang"] : $currentLang;

	// Update current lang
	$_SESSION["dynamicPayment"]["lang"] = $lang;
	
	// Read templates
	$templateMeta = $util->readTemplate("lang/template/meta.php");
	$templateHeader = $util->readTemplate("lang/template/header.php");
	$templateFooter = $util->readTemplate("lang/template/footer.php");
	$templatePaymentMenu = $util->readTemplate("lang/template/paymentmenu.php");
	$templateVideoMenu = $util->readTemplate("lang/template/videomenu.php");

	// Read language json
	$langMeta = $util->readLang("lang/meta.json");
	$langHeader = $util->readLang("lang/header.json");
	$langFooter = $util->readLang("lang/footer.json");
	$langPaymentMenu = $util->readLang("lang/paymentmenu.json");
	$langVideoMenu = $util->readLang("lang/videomenu.json");

	/**
	 * 	Meta
	 */
		$templateData["templateMeta"] = $util->langTemplate( $templateMeta, $langMeta, $lang );

	/**
	 * 	Header
	 */
		$headerActive["aboutUsActive"] = "";
		$headerActive["paymentMethodActive"] = "";
		$headerActive["solutionActive"] = "";
		$headerActive["clientsAndPartnerActive"] = "";
		$headerActive["contactUsActive"] = "";
		if ( isset($page) ) {
			$headerActive[$page."Active"] = "active_nav";
		}
		$templateHeader = $util->template($templateHeader, $headerActive);

		$templateData["templateHeader"] = $util->langTemplate($templateHeader, $langHeader, $lang);
		
	/**
	 * 	Footer
	 */

		$templateData["templateFooter"] = $util->langTemplate($templateFooter, $langFooter, $lang);

	/**
	 * 	Payment menu
	 */

		$templateData["templatePaymentMenu"] = $util->langTemplate($templatePaymentMenu, $langPaymentMenu, $lang);
	
	/**
	 * 	Video menu
	 */

		$templateData["templateVideoMenu"] = $util->langTemplate($templateVideoMenu, $langVideoMenu, $lang);

?>