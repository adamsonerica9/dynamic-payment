semantic.dropmenu = {};

// ready event
semantic.dropmenu.ready = function() {

  // selector cache
  var
    $examples         = $('.example'),
    $hoverdropmenu    = $examples.filter('.hover').find('.ui.dropmenu'),
    $buttondropmenu   = $examples.filter('.button.example').find('.ui.dropmenu'),
    $dropmenu         = $examples.filter('.dropmenu').find('.menu > .item > .ui.dropmenu, .menu > .item.ui.dropmenu, > .ui.dropmenu:not(.simple), .inline.dropmenu, .icon.buttons .button, .form .dropmenu.selection'),
    $transition       = $examples.filter('.transition').find('.ui.dropmenu'),
    $simpledropmenu   = $examples.filter('.simple').find('.ui.dropmenu'),
    $transitionButton = $examples.filter('.transition').find('.ui.button').first(),
    $categorydropmenu = $examples.filter('.category').find('.ui.dropmenu'),
    // alias
    handler
  ;

  // event handlers
  handler = {

  };

  $transitionButton
    .on('click', function(event) {
      $transition.dropmenu('toggle');
      event.stopImmediatePropagation();
    })
  ;

  $transition
    .dropmenu({
      onChange: function(value) {
        $transition.dropmenu('setting', 'transition', value);
      }
    })
  ;

  $categorydropmenu
    .dropmenu({
      allowCategorySelection: true
    })
  ;

  $dropmenu
    .dropmenu()
  ;
  $hoverdropmenu
    .dropmenu({
      on: 'hover',
      action: 'hide'
    })
  ;
  $simpledropmenu
    .dropmenu({
      action: 'hide'
    })
  ;
  $buttondropmenu
    .dropmenu({
      action: 'hide'
    })
  ;

};


// attach ready event
$(document)
  .ready(semantic.dropmenu.ready)
;