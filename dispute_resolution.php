<?php
	session_start();

	include "phpclass/util.php";
	include "lang.php";
	
	// Read templates
	$templateIndex = $util->readTemplate("lang/template/dispute_resolution.php");
	
	// Read language json
	$langIndex = $util->readLang("lang/dispute_resolution.json");
	
	/** 
	 * 	Page content
	*/
	$templateIndex = $util->langTemplate($templateIndex, $langIndex, $lang);

	$display = $util->template($templateIndex, $templateData);
	
	echo $display;

?>