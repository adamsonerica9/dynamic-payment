﻿<?php
	
	use PHPMailer\PHPMailer\PHPMailer;

	require 'vendor/autoload.php';
	
	$errorStr = "";
	$isMailError = false;
	
	// $emailTemplateId = addslashes($_REQUEST["emailTemplateId"]);
	$sql = "select * from emailTemplate where emailTemplateId ='".$emailTemplateId."'";
	$setting = $db->sql($sql)->fetch();
	
	$folder = @$_REQUEST["folder"];
	$smtp = $setting["smtp"];
	$from = $setting["email"];
	$fromName = $setting["fromName"];
	$username = $setting["username"];
	$password = $setting["password"];
	$port = $setting["port"];

	$redirect = $setting["url"];
	$email = $setting["mailTo"];
	$subject = $setting["subject"];
	$content = $setting["content"];
	
	foreach($mailParams as $key=>$value) {
		// replace markup with form data {$example}
		$subject = str_replace("{\$".$key."}",$value,$subject);
		$content = str_replace("{\$".$key."}",$value,$content);
		$email = str_replace("{\$".$key."}",$value,$email);
		$redirect = str_replace("{\$".$key."}",$value,$redirect);
	}
	
	try {
		$mail = new PHPMailer(true);
		$mail->isSMTP();
		
		$mail->CharSet	  = "UTF-8";
		$mail->Host       = $smtp;
		$mail->Port       = $port;
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth   = true;
		$mail->Username   = $username;
		$mail->Password   = $password;

		$mail->SetFrom($from, $fromName);
		$mail->addAddress($email);
		
		$mail->SMTPDebug   = 4;
		$mail->Debugoutput = function ($str, $level) { 
			global $errorStr;
			$errorStr .= $str."<br>";
		};
		$mail->IsHTML(true);
		$mail->SMTPOptions = array('tls' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true)); //remove this part once you get the SSL Certificate working

		$mail->Subject = $subject;
		$mail->Body    = $content;
		$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if (!$mail->send()) {
			$isMailError = true;
			$errorStr = 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			$isMailError = false;
		}
	} catch (Exception $e) {
		$isMailError = true;
		// save log into database
		$sql = "insert into emailLog values ( '', '".$emailTemplateId."' ,'".addslashes($errorStr)."' ,'".json_encode($setting)."' ,'".json_encode($_REQUEST)."', '".$from."', '".$email."' ,'".$subject."' ,'".$content."' ,'".date("Y-m-d, H:i:s",time())."' )";
		$db->sql($sql);
		die();
	}
	
	// save log into database
	$sql = "insert into emailLog values ( '', '".$emailTemplateId."' ,'No error' ,'".json_encode($setting)."' ,'".json_encode($_REQUEST)."', '".$from."', '".$email."' ,'".$subject."' ,'".$content."' ,'".date("Y-m-d, H:i:s",time())."' )";
	$db->sql($sql);
	
?>
