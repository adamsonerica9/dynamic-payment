<?php
	session_start();

	$page = "paymentMethod";

	include "phpclass/util.php";
	include "lang.php";
	
	// Read templates
	$templateIndex = $util->readTemplate("lang/template/pm_unionpay.php");
	
	// Read language json
	$langIndex = $util->readLang("lang/pm_unionpay.json");
	
	/** 
	 * 	Page content
	*/
	$templateIndex = $util->langTemplate($templateIndex, $langIndex, $lang);

	// Payment menu active
	$templateData["payment"] = "";
	$templateData["alipay"] = "";
	$templateData["unionpay"] = "active";
	$templateData["wechat"] = "";
	
	$display = $util->template($templateIndex, $templateData);
	
	echo $display;

?>