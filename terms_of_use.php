<?php
	session_start();

	include "phpclass/util.php";
	include "lang.php";
	
	// Read templates
	$templateIndex = $util->readTemplate("lang/template/terms_of_use.php");
	
	// Read language json
	$langIndex = $util->readLang("lang/terms_of_use.json");
	
	/** 
	 * 	Page content
	*/
	$templateIndex = $util->langTemplate($templateIndex, $langIndex, $lang);

	$display = $util->template($templateIndex, $templateData);
	
	echo $display;

?>