<?php
	session_start();

	$page = "clientsAndPartner";

	include "phpclass/util.php";
	include "lang.php";
	
	// Read templates
	$templateIndex = $util->readTemplate("lang/template/our_partners.php");
	
	// Read language json
	$langIndex = $util->readLang("lang/our_partners.json");
	
	/** 
	 * 	Page content
	*/
	$templateIndex = $util->langTemplate($templateIndex, $langIndex, $lang);

	$display = $util->template($templateIndex, $templateData);
	
	echo $display;

?>