<?php
	session_start();

	// Header menu active
	$page = "paymentMethod";

	include "phpclass/util.php";
	include "lang.php";
	
	// Read templates
	$templateIndex = $util->readTemplate("lang/template/payment_method.php");
	
	// Read language json
	$langIndex = $util->readLang("lang/payment_method.json");
	
	/** 
	 * 	Page content
	*/

	$templateIndex = $util->langTemplate($templateIndex, $langIndex, $lang);

	// Payment menu active
	$templateData["payment"] = "active";
	$templateData["alipay"] = "";
	$templateData["unionpay"] = "";
	$templateData["wechat"] = "";

	$display = $util->template($templateIndex, $templateData);
	
	echo $display;

?>